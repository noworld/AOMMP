#!/usr/bin/python2

from SimpleXMLRPCServer import SimpleXMLRPCServer
from SocketServer import ThreadingMixIn
from xmlrpclib import ServerProxy
import thread

class ThreadXMLRPCServer(ThreadingMixIn, SimpleXMLRPCServer):
    pass
class RPCServer():
    def __init__(self, ip, port='8000'):
        self.ip = ip
        self.port = int(port)
        self.svr = None

    def start(self, func_lst):
        thread.start_new_thread(self.service, (func_lst, 0,))

    def resume_service(self, v1, v2):
        self.svr.serve_forever(poll_interval=0.001)

    def service(self, func_lst, v1):
        self.svr = ThreadXMLRPCServer((self.ip, self.port), allow_none=True)
        for func in func_lst:
            self.svr.register_function(func)
        self.svr.serve_forever(poll_interval=0.001)

    def activate(self):
        thread.start_new_thread(self.resume_service, (0, 0,))

    def shutdown(self):
        try:
            print ("shutdown now !")
            self.svr.shutdown()
        except Exception, e:
            print 'rpc_server shutdown:', str(e)


