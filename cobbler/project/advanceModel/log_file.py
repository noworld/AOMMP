import InitConfig as config


a = config.Config()
log_path = a.getLogPath()

def writeTextLog(contType,content="",ip=None, port=None, cobbled_status=None,sendType=None):
    """
    write to logger file for the all message
    :param contType:  log type
    :param content:   log content
    :param ip:    ip
    :param port:  port
    :param cobbled_status:   cobbler status
    :return:
    """
    file = log_path
    f = open(file, 'a')
    try:
        if contType == "java":
            f.write(content)
        elif contType == "regist":
            if cobbled_status == True:
                f.write("cobbler service start and regist cobblerd server to web project status is %s\n"%(sendType))
            elif cobbled_status == False:
                f.write("service cobblerd start error!\n")
            else:
                f.write("get cobblerd status error!\n")
        elif contType == "download" or contType =="import" or contType =="removeOS" or contType =="umount" or contType =="syncKs":
            f.write("%s\n"%(content))
        elif contType =="synKs":
             f.write("%s\n"%(content))
        else:
            f.write("unknown contType:%s\n"%(contType))
    finally:
            f.close()
