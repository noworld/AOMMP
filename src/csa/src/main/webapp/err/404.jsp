<%@ page language="java" pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>抱歉！页面无法访问...</title>
<style>
*{ padding:0px; margin:0px;}
body{ font-family: Microsoft YaHei , SimSun , SimHei; color:#393939;}
#error{ margin:0px auto; width:450px;}
#error .content{background:url(/csa/err/images/error-404.png) no-repeat left center; padding-left:140px;margin-top:180px; height:150px;}
#error .content .p_t{font-size:28px; padding:5px 0 0 10px;}
#error .content .p_c{font-family:Arial; font-size:18px; color:#777777; padding:5px 0 0 10px;}
#error .content .p_c span{ font-weight:bold; font-size:21px; color:#393939; }
#error .content .p_b{font-size:21px; padding:20px 0 0 10px;}
</style>
</head>

<body>
	<div id="error" style=" ">
    	<div class="content">
        	<p class="p_t">抱歉！页面无法访问...</p>
            <p class="p_c"><span>404.</span> That’s an error.</p>
            <p class="p_b">请检查您访问的网址是否正确</p>
        </div>
    </div>
</body>
</html>

