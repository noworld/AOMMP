<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <base href="<%=basePath%>">
    
   
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	--><link href="static/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="static/css/webuploader/webuploader.css">
		<link href="static/css/base.css" rel="stylesheet" type="text/css" />
		<link href="static/css/main.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="static/css/sweetalert.css"/>
<title>CAS - 首页 - OS部署配置IP</title>
<link href="static/css/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="static/css/base.css" rel="stylesheet" type="text/css" />
<link href="static/css/main.css" rel="stylesheet" type="text/css" />
<script src="static/js/sweetalert.min.js" type="text/javascript"></script>
<script src="static/js/sweetalert-dev.js" type="text/javascript"></script>
<script type="text/javascript" src="static/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="static/js/effects.js"></script>
	
<script type="text/javascript">
 			$(document).ready(function(){
 		
			$.ajax({
		        type:'POST',
		        dataType:'json',
		        url:'getAllKs',
		        success:function(data){
		        	$.each(data.kickStartList, function(i) {
		        		$('#diqucnt ul').append('<li><a csaid="'+this.csaId+'" href="javascript:void(0)">'+this.ksName+'</a></li>');
		        	});
		        	var groupNum = 1;
		        	$.each(data.ipList, function(j) {
		        		if($('#batChid_'+this.batChid).length>0){
		        			$('#batChid_'+this.batChid+' ul').append('<li><a csaid="'+this.csaId+'" href="javascript:void(0)">'+this.ipAddress+'</a></li>');
		        		}else{
		        			if(groupNum!=1){
			        			$('#tab1 .oscntabc_1').append('<div id="batChid_'+this.batChid+'" class="oscntabc_close"><h4>'+this.batChid+'</h4><ul><li><a csaid="'+this.csaId+'" href="javascript:void(0)">'+this.ipAddress+'</a></li></ul></div>');
		        			}else{
			        			$('#tab1 .oscntabc_1').append('<div id="batChid_'+this.batChid+'" class="oscntabc_open"><h4>'+this.batChid+'</h4><ul><li><a csaid="'+this.csaId+'" href="javascript:void(0)">'+this.ipAddress+'</a></li></ul></div>');
		        			}
		        			groupNum = groupNum+1;
		        		}
		        	});
		        }
		        });
		   $('.bodrad5.bg_white.oscntabc').on("click",".oscntabc_1 li",function(){
		 	 
			var text = $(this).text();
			var ipId = $(this).attr('csaid');
			var boolean = true;
			$('.bodrad5.bg_white.oscnt_r ul li').each(function(){
			    if($(this).text()==text){
			    	boolean=false;
			    }
			});
			if(boolean){
				$('.bodrad5.bg_white.oscnt_r ul').append('<li><input name="ipGroup" type="hidden" value="'+ipId+'"><a href="javascript:void(0)">'+text+'<s></s></a></li>');
			}
			
		});
		
				//展开折叠应用范围列表
				$('.oscntabc_1').on("click","h4",function(){
				if($(this).parent().attr('class')=='oscntabc_open'){
					$(this).parent().attr('class','oscntabc_close');
				}else if($(this).parent().attr('class')=='oscntabc_close'){
					$(this).parent().attr('class','oscntabc_open');
				}
				});
			//应用范围折叠展示
			 	$(document).on("click",'a[role="button"]', function() {
				var span = $(this).find('span');
				if(span.attr('class')=='glyphicon glyphicon-triangle-right'){
					span.attr('class','glyphicon glyphicon-triangle-bottom');
					$(this).parent().parent().next().css("display","");
				}else{
					span.attr('class','glyphicon glyphicon-triangle-right');
					$(this).parent().parent().next().css("display","none");
				}
				}); 
				
				//点击应用范围内ip添加到右侧已选列表
		$('.bodrad5.bg_white.oscntabc').on("mouseover mouseout click","li",function(event){
							
			 if(event.type == "mouseover"){
				 $(this).addClass('current');
			 }else if(event.type == "mouseout"){
				 $(this).removeClass('current');
			 }else if(event.type == "click"){
			 $('.oscnt.fr h3').html('已选IP数量: '+$('.bodrad5.bg_white.oscnt_r ul li').length);
			 }  	
		})
		$('.oscnt.fr').on("mouseover mouseout","li",function(event){
			
			 if(event.type == "mouseover"){
				 $(this).addClass('current');
			 }else if(event.type == "mouseout"){
				 $(this).removeClass('current');
			 }
		})
		//取消已选ip
		$('.oscnt.fr').on("click","li",function(){
				
			$(this).remove();
			$('.oscnt.fr h3').html('已选IP数量: '+$('.bodrad5.bg_white.oscnt_r ul li').length);
		});		
		}); 
	 
		var allIpaddress = new Array();
		var allMask = new Array();
		var allMac = new Array();
	//查询ip是否重复  
	 function tjall(){
	//ip
		var ipaddress =$("input[name='idaddress1']").val()+"."+$("input[name='idaddress2']").val()+"."+$("input[name='idaddress3']").val()+"."+$("input[name='idaddress4']").val(); 
	//掩码
		var mask =$("input[name='mask1']").val()+"."+$("input[name='mask2']").val()+"."+$("input[name='mask3']").val()+"."+$("input[name='mask4']").val();
	//mac 	
		var mac = $("input[name='mac']").val();
		//var val=/^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$/;
		 $.ajax({
	     type: 'POST',
	     url: "findIp" ,
	     data: {
	     	"ipaddress":ipaddress
	     },
	     success: function(data){
	   	  var ipresult=data;
	     	// setTableValue(data);
	    	if(ipresult==false){
	    		$('.oscnt.fr ul').append('<li><a href="javascript:void(0)">'+ipaddress+'<s></s></a></li>');
				 $('.oscnt.fr h3').html('已选IP数量: '+$('.bodrad5.bg_white.oscnt_r ul li').length);
				var itemsipaddress = allIpaddress.push(ipaddress);
				var itemsmask = allMask.push(mask);
				var itemsmask = allMac.push(mac);
			
			
			 $('.xuliehua1 input').val("");   //IP添加好刷新表单
			}else{
	    			 swal("IP已存在，请重新输入!", "", "error");
	    	}
	     } ,
	     error: function(data){
	 		//swal(msg.FindFileError, "", "warning");
	     },
	     dataType: "json"
	});		
		} 
			
	 	 function focusOut(obj){
					
					var flag =false; 
					var id=obj.getAttribute("id");
					if(id=="batchid"){
							if(obj.value==""){
							$(".xuliehua1 ul li:eq(0)").append('<p class="error_tip">批次不能为空！</p>');
							}else{	
							$(".xuliehua1 ul li:eq(0) p").remove();
							}
					} /* else if(id=="idaddress1"||id=="idaddress2"||id=="idaddress3"||id=="idaddress4"){
						var ipaddress =$("input[name='idaddress1']").val()+"."+$("input[name='idaddress2']").val()+"."+$("input[name='idaddress3']").val()+"."+$("input[name='idaddress4']").val();
							
					var pattern =/^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$/;
						if(obj.value==""){
						$(".xuliehua1 ul li:eq(2)").append('<p class="error_tip">批次不能为空！</p>');
							alert("ip不能为空");
						}
					} */
		
		}  
//====================================================================================================
 /*     $(function () {
            $("xuliehua1").validate({
                rules: {
                    UserName: { required: true, minlength: 3, maxlength: 18, remote: "/Home/CheckUserName" },
                    Email: { required: true,email:true },
                    UserPassword: { required: true ,minlength: 6 },
                    Mobile: { required: true, number:true },
                    IdCard: { required: true,isIdCardNo: true },
                    Age: { required: true ,number:true,min:1,max:100 }
                },
                messages:{
                    UserName: { 
                        required: "请输入用户名！",
                        minlength: "用户名长度最少需要3位！",
                        maxlength: "用户名长度最大不能超过18位！",
                        remote: "此用户名已存在！"
                     },
                    Email: {
                        required: "请填写邮箱",
                        email: "请输入正确的邮箱格式"
                    },
                    UserPassword: {
                        required: "请填写你的密码!",
                        minlength: "密码长度不能小于6位"
                    },
                    Mobile: {
                        required: "请填写你的手机号码",
                        number:"手机号码只能为数字"
                     },
                    IdCard: {
                        required: "请输入身份证号码！",
                        isIdCardNo:"请输入正确的身份证号码！"
                    },
                    Age: {
                        required: "请输入年龄!",
                        number: "请输入数字",
                        min: "年龄不能小于1", 
                        max: "年龄不能大于100" 
                    }
                },
                错误提示位置
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent());
                }
            })
        }) */
//关闭窗口					
 function close1(){
  			window.close();
 }
 
 //div点击隐藏切换
 function st1_nextPage(){
		 $("#1st").css("display","none");
 		$("#2st").css("display","block");
 }
 function st2_latterPage(){
		 $("#1st").css("display","block");
 		$("#2st").css("display","none");
 }
 function st2_nextPage(){
		 $("#2st").css("display","none");
 		$("#3st").css("display","block");
 }
 function st3_latterPage(){
 		$("#3st").css("display","none");
 		$("#2st").css("display","block");
 }
 function st3_nextPage(){
 		$("#3st").css("display","none");
 		$("#4st").css("display","block");
 }

 
</script>
</head>

<body>

<div class="home_os" id="1st" style="position:absolute;z-index:999;">  <!-- 第一页 -->
	<h2>OS部署目标IP</h2>
    <div class="home_oscnt clearfix">
    	<div class="oscnt fl">
            <div  class="oscntab">
                <ul>
                    <li class="current" title="tab1">现有IP中选择</li>
                    <li title="tab2" >添加新主机</li>
                </ul>
            </div>
            <div class="bodrad5 bg_white oscntabc">
            	<div id="tab1">
               	<div class="oscntabc_1">
                      <!-- li分组IP数据 -->
                    </div>  
                </div>
                <div id="tab2" style="display:none">
                	<div class="oscnt_ipadd">
                		<form class="xuliehua1">
                        <ul>
                            <li class="error"><b>批次</b><div class="ipfpcn"><input id="batchid" class="inptext" type="text" value="" name="batchid"  onblur="focusOut(this)"></div>
                          <!-- <p class="error_tip">错误提示</p>  -->
                            </li>
                            
                            <li name="guanLiIp"><b>管理IP</b><div class="ipfpcn"><span><input class="inptext" type="text" value="" name="idaddress1" id="idaddress1" onblur="focusOut(this)"></span><em>.</em><span><input class="inptext" type="text" value="" name="idaddress2"  id="idaddress2" onblur="focusOut(this)"></span><em>.</em><span><input class="inptext" type="text" value="" name="idaddress3" id="idaddress3" onblur="focusOut(this)"></span><em>.</em><span><input class="inptext" type="text" value="" name="idaddress4" id="idaddress4" onblur="focusOut(this)"></span></div>
                            
                            </li>
                            <li>
                            <b>掩码</b><div class="ipfpcn"><span><input class="inptext" type="text" value="" name="mask1" onblur="focusOut(this)"></span><em>.</em><span><input class="inptext" type="text" value="" name="mask2" onblur="focusOut(this)"></span><em>.</em><span><input class="inptext" type="text" value="" name="mask3" onblur="focusOut(this)"></span><em>.</em><span><input class="inptext" type="text" value="" name="mask4" onblur="focusOut(this)"></span></div>
                            </li>
                            <li>
                            <b>MAC</b><div class="ipfpcn"><input class="inptext" type="text" value="" name="mac" placeholder="请输入MAC" onblur="focusOut(this)"></div>
                           </li>
                        
                        </ul>
                        </form>
                        <div class="casbtn"><a class="whitebtn" href="javascript:void(0)">取消</a><a class="tianjiaflash" href="javascript:void(0)" onclick="tjall()">添加</a></div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="oscnt fr">
        	<h3>已选IP</h3>
            <div class="bodrad5 bg_white oscnt_r ">
             <ul>
                	<!-- <li><a href="#">192.168.1.1<s></s></a></li>
                    <li class="current"><a href="#">192.168.1.1<s></s></a></li>
                    <li><a href="#">192.168.1.1<s></s></a></li>
                    <li><a href="#">192.168.1.1<s></s></a></li> -->
                </ul> 
            </div>
        </div>
        
        <div class="casbtn"><a class="bluebtn200" href="javascript:void(0)" onclick="st1_nextPage()">下一步</a></div>
    </div>
</div>
<div class="dierye" id="2st" style="position:absolute;z-index:998;display:none;">   <!-- 第二页 -->
<div class="home_os">
	<h2>OS布署文件</h2>
    <div class="home_oscnt">
    	<div class="osbspz">
        	<div class="osbstab">
            	<ul>
                	<li class="current" title="osbstab1">从已有的OS布署中选择</li><li title="osbstab2">添加新的OS布署文件</li>
                </ul>
            </div>
            <div>
            <div id="osbstab1" style="display:block">
                <ul class="osbssearch">
                    <li class="error"><label class="datemod"><input type="text" value="" placeholder="b/AirPay.Action"><s class="down"></s></label>
                <!--   <p class="error_tip">错误提示</p> -->
                    
                    <div class="menu2" id="diqucnt" style="display:block;">
                    <s class="up"></s>
                 <!--    <ul class=" bodrad5">
                       <li><a href="#">全省</a></li>
                        <li><a href="#">成都市</a></li>
                        <li><z`a href="#">阿坝藏族羌族自阿坝藏族羌族自治州治州</a></li>
                        <li><a href="#">甘孜藏族自治州</a></li>
                        <li><a href="#">攀枝花市</a></li>
                        <li><a href="#">绵阳市</a></li>
                    </ul> -->
                    </div>
                    
                    </li>
                </ul>
                <div class="osbspzcn">
                    <ul class="osbspztab">
                        <li class="current" title="tab1">基本信息<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                        <li title="tab2">分区信息<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                        <li title="tab3">软件包选择<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                        <li title="tab4">安装后执行脚本<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                    </ul>
                    <div>
                        <div class="baseinfo" id="tab1" style="display:block">
                            <ul>
                                <li><b>语言</b><div class="infoli">中文</div></li>
                                <li><b>时区</b><div class="infoli">上海</div></li>
                                <li><b>根口令</b><div class="infoli">123456</div></li>
                                <li><b>目标系统</b><div class="infoli">x86,AMD64或Inter EM64T<br>
                                    <p class="mt10"><input type="checkbox" checked> 安装后重新引导系统</p>
                                </div></li>
                                <li>&nbsp;</li>
                            </ul>
                        </div>
                        <div class="fenquinfo" id="tab2" style="display:none">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <th scope="col" width="30%">挂载点/RAID</th>
                                <th scope="col" width="30%">类型</th>
                                <th scope="col" width="20%">大小</th>
                                <th scope="col" width="20%">操作</th>
                              </tr>                      
                              <tr>
                                <td>/</td>
                                <td>ext4</td>
                                <td>100000</td>
                                <td><a class="tdedit" href="#">编辑</a><a class="tddel" href="#">删除</a></td>
                              </tr>
                              <tr>
                                <td>/home</td>
                                <td>ext4</td>
                                <td>100000</td>
                                <td><a href="#">编辑</a><a href="#">删除</a></td>
                              </tr>
                              <tr>
                                <td>/home</td>
                                <td>ext4</td>
                                <td>100000</td>
                                <td><a href="#">编辑</a><a href="#">删除</a></td>
                              </tr>
                            </table>
                            <div class="fenquadd" id="osbs2"><a href="#"><s></s>添加</a></div>
                        </div>
                        <div class="softinfo" id="tab3" style="display:none">
                            <ul class="softinfo_l">
                                <li><a href="#">基本系统</a></li>
                                <li><a href="#">应用程序</a></li>
                                <li><a href="#">开发</a></li>
                                <li class="current"><a>服务器</a></li>
                                <li><a href="#">桌面环境</a></li>
                                <li><a href="#">虚拟化</a></li>
                                <li><a href="#">语言支持</a></li>
                            </ul>
                            <ul class="softinfo_r">
                                <li><input type="checkbox"  value=""> DNS 名称服务器</li>
                                <li><input type="checkbox"  value=""> MySQL  服务器</li>
                                <li><input type="checkbox"  value=""> PostgreSQL  数据库</li>
                                <li><input type="checkbox"  value=""> Windows  文件服务器</li>
                                <li><input type="checkbox"  value=""> 万维网服务器</li>
                                <li><input type="checkbox"  value=""> 打印支持</li>
                                <li><input type="checkbox"  value=""> 新闻服务器</li>
    
                            </ul>
                        </div>
                        <div class="performinfo clearfix" id="tab4" style="display:none">
                            <h3>脚本命令</h3>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <th scope="col" width="50%">名称</th>
                                <th scope="col" width="50%">大小</th>
                              </tr>                      
                              <tr>
                                <td>reateUser.sh</td>
                                <td>3K</td>
                              </tr>
                              <tr>
                                <td>syncTime.sh</td>
                                <td>1K</td>
                              </tr>
                              <tr>
                                <td>installoracle.sh</td>
                                <td>1,234K</td>
                              </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="osbstab2" style="display:none">
                <ul class="osbssearch">
                	<li class="error"><b>配置名称</b><input class="osbsinp" type="text" value="" placeholder="b/AirPay.Action"><p class="error_tip" style="left:80px;">错误提示</p></li>
                    <li><b>镜像名称</b><label class="datemod"><input type="text" value="" placeholder="b/AirPay.Action"><s class="down"></s></label></li>
                </ul>
                <div class="osbspzcn">
            	<ul class="osbspztab">
                	<li class="current" title="osbstab2-1">基本信息<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                    <li title="osbstab2-2">分区信息<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                    <li title="osbstab2-3">软件包选择<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                    <li title="osbstab2-4">安装后执行脚本<div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
                </ul>
            	<div>
                	<div class="baseinfo" id="osbstab2-1" style="display:block">
                    	<ul>
                        	<li><b>语言</b><div class="infoli"><input type="text" value=""></div><p class="error_tip">语言填写错误</p></li>
                            <li class="error"><b>时区</b><div class="infoli"><input type="text" value=""></div><p class="error_tip">时区填写错误</p></li>
                            <li><b>根口令</b><div class="infoli"><input type="text" value=""></div></li>
                            <li><b>确认口令</b><div class="infoli"><input type="text" value=""><span class="ml10"><input type="checkbox" checked> 随机生成</span></div></li>
                            <li><b>目标系统</b><div class="infoli"><input type="text" value=""><br>
                            	<p class="ml10 mt10"><input type="checkbox" checked> 安装后重新引导系统</p>
                            </div></li>
                            <li>&nbsp;</li>
                        </ul>
                    </div>
                    <div class="fenquinfo" id="osbstab2-2" style="display:none">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <th scope="col" width="30%">挂载点/RAID</th>
                            <th scope="col" width="30%">类型</th>
                            <th scope="col" width="20%">大小</th>
                            <th scope="col" width="20%">操作</th>
                          </tr>                      
                          <tr>
                            <td>/</td>
                            <td>ext4</td>
                            <td>100000</td>
                            <td><a class="tdedit" href="#">编辑</a><a class="tddel" href="#">删除</a></td>
                          </tr>
                          <tr>
                            <td>/home</td>
                            <td>ext4</td>
                            <td>100000</td>
                            <td><a href="#">编辑</a><a href="#">删除</a></td>
                          </tr>
                          <tr>
                            <td>/home</td>
                            <td>ext4</td>
                            <td>100000</td>
                            <td><a href="#">编辑</a><a href="#">删除</a></td>
                          </tr>
                        </table>
						<div class="fenquadd"><a href="#"><s></s>添加</a></div>
                    </div>
                    <div class="softinfo" id="osbstab2-3" style="display:none">
                    	<ul class="softinfo_l">
                        	<li><a href="#">基本系统</a></li>
                            <li><a href="#">应用程序</a></li>
                            <li><a href="#">开发</a></li>
                            <li class="current">服务器</a></li>
                            <li><a href="#">桌面环境</a></li>
                            <li><a href="#">虚拟化</a></li>
                            <li><a href="#">语言支持</a></li>
                        </ul>
                        <ul class="softinfo_r">
                        	<li><input type="checkbox"  value=""> DNS 名称服务器</li>
                            <li><input type="checkbox"  value=""> MySQL  服务器</li>
                            <li><input type="checkbox"  value=""> PostgreSQL  数据库</li>
                            <li><input type="checkbox"  value=""> Windows  文件服务器</li>
                            <li><input type="checkbox"  value=""> 万维网服务器</li>
                            <li><input type="checkbox"  value=""> 打印支持</li>
                            <li><input type="checkbox"  value=""> 新闻服务器</li>

                        </ul>
                    </div>
                    <div class="performinfo clearfix" id="osbstab2-4" style="display:none">
                    	<h3>选择安装后执行脚本</h3>
                        <div class="perinfo_l fl">
                        	<div class="perinfo_open">
                        	<h4>分组1</h4>
                        	<ul>
                            	<li><a href="#">shell1.sh</a></li>
                                <li class="current"><a href="#">shell2.sh</a></li>
                                <li><a href="#">shell3.sh</a></li>
                                <li><a href="#">shell4.sh</a></li>
                            </ul>
                            </div>
                            <div class="perinfo_close">
                        	<h4>分组2</h4>
                        	<ul>
                            	<li><a href="#">shell1.sh</a></li>
                                <li class="current"><a href="#">shell2.sh</a></li>
                                <li><a href="#">shell3.sh</a></li>
                                <li><a href="#">shell4.sh</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="perinfo_l fr">
                        	<ul>
                            	<li><a href="#">shell1.sh</a></li>
                                <li class="current"><a href="#">shell2.sh</a></li>
                                <li><a href="#">shell3.sh</a></li>
                                <li><a href="#">shell4.sh</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
        <div class="casbtn">
        <a class="whitebtn" href="javascript:void(0)" onclick="st2_latterPage()">上一步</a><a href="javascript:void(0)" onclick="st2_nextPage()">下一步</a>
        </div>
    </div>
</div>


<div id="osbs2_cnt" style="display:none">
<div class="popdiv-mask"></div>
<div class="popdiv">
	<h2>添加分区<a class="ico_close" href="#"></a></h2>
    <div class="popdiv_cnt">
    	<ul class="popdiv_iso2 pop_os1">
        	<li><b>挂载点</b><span><label class="datemod"><input type="text" value="" /><s class="down"></s></label></span></li>
            <li><b>文件系统类型</b><span><label class="datemod"><input type="text" value="" /><s class="down"></s></label></span></li>
            <li><b>大小(MB)</b><div class="iso2"><input class="inptxt_pop" type="text" value="" /></div></li>
            <li><b>其他大小选项</b><div class="iso3">
            	<p><input type="radio" value=""> 固定大小</p>
                <p><input type="radio" value=""> 使用空间的最大限度(MB) <input type="text"></p>
                <p><input type="radio" value=""> 会用磁盘上全部为用空间</p>
                <p><input type="radio" value=""> 使用不推荐的交换区大小</p>
            </div></li>
            <li class="pop_error">错误提示</li>
        </ul>
    </div>
    <div class="popdiv_btn"><a class="btnblue bodrad5" href="#">添加</a></div>
</div>
</div>
</div>
<div class="home_os" id="3st" style="position:absolute;z-index:997;display:none"><!-- 第三页 -->
	<h2>安装确认</h2>
    <div class="home_oscnt">
    	<ul class="install">
        	<li><b>所选 IP：</b><div class="installst bodrad5 bg_white">192.168.101.220<br>192.168.111.222</div></li>
            <li><b>OS布署配置：</b><div>rhrl6</div></li>
        </ul>
        <div class="casbtn"><a class="whitebtn" href="javascript:void(0)" onclick="st3_latterPage()">上一步</a><a href="javascript:void(0)" onclick="st3_nextPage()">安装</a></div>
    </div>
</div>

<div class="home_os" id="4st" style="position:absolute;z-index:996;display:none">		<!-- 第四页 -->
	<h2>安装开始...</h2>
    <div class="home_oscnt">
    	<div class="install_star">
        	<h3>保存OS部署方案成功</h3>
            <h3>安装开始......</h3>
            <ul>
            	<li class="install_suc">192.168.101.220<span><em></em>安装日志</span></li>
            	<li class="install_fail">192.168.101.220<span><em></em>安装日志</span></li>
            	<li class="install_ing">192.168.101.220<span><em></em>安装日志</span></li>
            	<li>192.168.101.220<span><em></em>等待安装</span></li>
                <li>192.168.101.220<span><em></em>等待安装</span></li>
                <li>192.168.101.220<span><em></em>等待安装</span></li>
                <li>192.168.101.220<span><em></em>等待安装</span></li>
            </ul>
        </div>
        <div class="casbtn"><a class=" bluebtn200" href="JavaScript:void(0)" onclick="close1()">关闭</a></div>
    </div>
</div>

</body>
</html>
