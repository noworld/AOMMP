 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CSA</title>
<link href="static/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- <link href="static/css/bootstrap/css/bootstrap.css" type="text/css" /> -->
<link href="static/css/base.css" rel="stylesheet" type="text/css" />
<link href="static/css/main.css" rel="stylesheet" type="text/css" />

<style>
.pages {
	text-align: center;
	margin: 30px 0;
}

.pages li a {
	font-size: 14px
}
.baseinfo ul li .infoli textarea {
	width: 100%;
	height: 60px;
	line-height: 20px;
	font-size: 12px;
	color: #888;
	padding: 0 5px;
	border: 1px solid #e6e6e6;
	border-radius: 5px;
}
</style>

<link href="static/css/sweetalert.css" rel="stylesheet" type="text/css"/>

<script src="static/js/sweetalert.min.js"></script>

<!-- 基础版本的jquery -->
<script type="text/javascript" src="static/js/jquery-2.2.1.min.js"></script>

<script src="static/css/bootstrap/js/bootstrap.min.js"
	type="text/javascript"></script>
<!-- 公共的js -->
<script type="text/javascript" src="static/js/currencyJs.js"></script>
<!-- jquery验证 -->
<script type="text/javascript" src="static/js/jquery.validate.js"></script>

<!-- jquery自定义验证方法 -->
<script type="text/javascript" src="static/js/additional-methods.js"></script>


<!-- bootstrap输入提示 -->
<script type="text/javascript" src="static/js/Bootstrap-3-Typeahead.js"></script>
<!-- 搜索组件 -->
<script type="text/javascript" src="static/js/search.js"></script>

<!-- 分页 -->
<script type="text/javascript" src="static/js/page.js"></script>
<!-- os 部署Js主文件 -->
<script type="text/javascript" src="static/js/osDeploy.js"></script>	

</head>
<body>
	<div style="display: none">
		<input type="hidden" id="bathPath" value="${pageContext.request.contextPath}" />
		<input type="hidden" id="optionType"/>
		<input type="hidden" id="trIndex"/>
		<input type="hidden" id="ksOption"/>
		<input type="hidden" id="csaId"/>
		<input type="hidden" id="tr_os_table"/>
		<input type="hidden" id="curPage" value="${curPage }" /> 
		<input type="hidden" id="showPage" value="${showPage }" /> 
		<input type="hidden" id="showCount" value="${showCount }" />
		<input type="hidden" id="totalCount" value="${totalCount }" />
	</div>

	<jsp:include page="../include/nav.jsp">
		<jsp:param value="deploy" name="menu" />
		<jsp:param value="osdeploy" name="menusub" />
	</jsp:include>
	<div id="main_content" class="content">
		<div class="btn_search">
			<div class="csa_btn">
				<a href="javascript:void(0)" id="editKs" onclick="addOrUpdate('add')"><spring:message
						code="AddKickstart"></spring:message></a>
			</div>
			<div class="csa_search bodrad5 bg_white">
				<input type="text" id="ks_search" value="" placeholder='<spring:message code="ksSearchTip"></spring:message>'/><a
					href="javascript:void(0)"></a>
			</div>
		</div>
		<div id="os_div" class="csa_list bodrad5 bg_white">
			<div class="csa_table">
				<div id="optionDiv" class="csa_tabletop" style="visibility:hidden;"><span id="selectedlen">已选 3 个文件</span>
				<a id="copyKs" class="bodrad5 edit"
						href="javascript:void(0)" onclick="copyKickstart()"><spring:message
							code="copyKs"></spring:message></a><a id="deleteIps" class="bodrad5 edit"
						href="javascript:void(0)" onclick="deleteKickstarts()"><spring:message
							code="delete"></spring:message></a><a id="update" class="bodrad5"
						href="javascript:void(0)" onclick="addOrUpdate('update')"
						style="disabled:disabled"><spring:message code="update"></spring:message></a>
				</div>
				<table id="os_table" width="100%" border="0" cellspacing="0"
					cellpadding="0">
					<%-- <tr>
						<th scope="col"><input type="checkbox" id="allCheck" value=""
							onclick="selectCheckBox(this)" /> <spring:message
								code="ConfigName"></spring:message></th>
						<th scope="col"><spring:message code="IsoName"></spring:message></th>
						<th scope="col"><spring:message code="Creator"></spring:message></th>
						<th scope="col"><spring:message code="CreationTime"></spring:message></th>
						<th scope="col"><spring:message code="Remark"></spring:message></th>
					</tr> --%>
					<%-- <c:forEach var="kickstart" items="${KsList}" varStatus="count">
						<tr>
							<td><input type="checkbox" id="${kickstart.csaId }"
								value="${kickstart.ksName }" onclick="selectCheckBox(this)" />${kickstart.ksName }</td>
							<td>${kickstart.iso.isoName }</td>
							<td>${kickstart.userAccount }</td>
							<td><fmt:formatDate value="${kickstart.createTime }"
									pattern="yyyy/MM/dd" /></td>
							<td><a class="bodrad5 edit" href="javascript:void(0)"
								onclick="viewDetail(this)"><spring:message
										code="updateHistory"></spring:message></a></td>
						</tr> 
					</c:forEach>--%>
				</table>
			</div>

		</div>
		<div class="pages">
			<div id="callBackPager"></div>
		</div>
	</div>



<div id="ks_content" class="home_os" style="display:none">
	<h2><spring:message code="addOSConfig"></spring:message></h2>
	<form id="signupForm1" method="post" class="form-horizontal" action="">
	    <div class="home_oscnt">
	    	<div class="osbspz">
	    	<ul class="osbssearch">
                <li><b><spring:message code="ConfName"></spring:message></b><input class="osbsinp" id="conf_name" name="conf_name" type="text" value="" placeholder="<spring:message code='ksSearchTip'></spring:message>"></li>
                <li><b><spring:message code="osname"></spring:message></b><input id="iso_name" name="iso_name" class="osbsinp" type="text" value=""placeholder="<spring:message code='osNull'></spring:message>"></li>
            </ul>
	            <div class="osbspzcn">
	            	<ul class="osbspztab">
	                	<li class="current" title="tab1"><spring:message code="basicMsg"></spring:message><div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
	                    <li title="tab2"><spring:message code="discMsg"></spring:message><div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
	                    <li title="tab3"><spring:message code="packMsg"></spring:message><div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
	                    <li title="tab4"><spring:message code="postMsg"></spring:message><div class="formtips formtips-top info"><em>◆</em><i>◆</i></div></li>
	                </ul>
	            	<div>
	                	<div class="baseinfo" id="tab1" style="display:block">
	                    	<ul>
	                        	<li><b><spring:message code="lanMsg"></spring:message></b>
		                        	<div class="infoli">
			                        	<select name="lan_in" id="lan_in" class="form-control">
	 										<option value="zh_CN"><spring:message code="chinese"></spring:message></option>
											<option value="en_US"><spring:message code="english"></spring:message></option>
	                					</select>
									</div>
								</li>
	                            <li><b><spring:message code="timezone"></spring:message></b>
		                            <div class="infoli">
			                            <select name="time_zone" id="time_zone" class="form-control">
											<option value="Asia/Shanghai"><spring:message code="shanghai"></spring:message></option>
											<option value="Asia/Chongqing" ><spring:message code="chongqing"></spring:message></option>
											<option value="Asia/Hong Kong" ><spring:message code="hongkong"></spring:message></option>
											<option value="Asia/Taipei" ><spring:message code="taipei"></spring:message></option>
										</select>
		                            </div>
	                            </li>
	                            <li><b><spring:message code="rootpwd"></spring:message></b><div class="infoli"><input id="root_pwd" name="root_pwd" type="text" value="" placeholder="<spring:message code='rootpwd'></spring:message>"></div></li>
	                            <li><b><spring:message code="conpwd"></spring:message></b><div class="infoli"><input id="root_pwd_confirm" name="root_pwd_confirm" type="text" placeholder="<spring:message code='conpwd'></spring:message>" value=""><span class="ml10"><input type="checkbox" id="radom_pwd" name="radom_pwd" onclick="rodom_check(this)"><spring:message code="radomPwd"></spring:message></span></div></li>
	                            <li >
	                            	<b></b>
	                            	<div class="infoli">
	                            		<p class="ml10 mt10"><input type="checkbox" id = "reboot_ch" name="reboot_ch"><spring:message code="reboot"></spring:message></p>
	                            	</div>
	                            </li>
	                            <li>&nbsp;</li>
	                        </ul> 
	                    </div>
	                    <div class="fenquinfo" id="tab2" style="display:none">
	                    	<table id="disc_tb" width="100%" border="0" cellspacing="0" cellpadding="0">
	                          <tr> 
	                            <th scope="col" width="30%"><spring:message code="RAID"></spring:message></th>
	                            <th scope="col" width="30%"><spring:message code="sysType"></spring:message></th>
	                            <th scope="col" width="20%"><spring:message code="size"></spring:message></th>
	                            <th scope="col" width="20%"><spring:message code="option"></spring:message></th>
	                          </tr>                      
	                        </table>
							<div class="fenquadd"><a href="javascript:void(0)" onclick="editSystemArea('add')"><s></s><spring:message code="add"></spring:message></a></div>
	                    </div>
	                    <div class="performinfo clearfix" id="tab3" style="display:none">
	                    	<h3><spring:message code="packMsg"></spring:message></h3>
	                        <div class="perinfo_l fl" id="packages_sou">
									<c:forEach items="${packages}" var="packagesGroup">
										<div class="perinfo_close">
										<h4>${packagesGroup.key }</h4>
											<ul>
												<c:forEach items="${packagesGroup.value}" var="package1">
			        								<li title = "${package1.csaId }"><a href="javascript:void(0)">${package1.packageName }</a></li>
												</c:forEach>
											</ul>
										</div>
									</c:forEach>
							</div>
	                        <div class="perinfo_l fr" id="packages_tar">
	                        	<ul>
	                        		
	                            </ul>
	                        </div>
	                    </div>
	                    <div class="performinfo clearfix" id="tab4" style="display:none">
	                    	<h3><spring:message code="postMsg"></spring:message></h3>
	                        <div class="perinfo_l fl" id="shl_sou">
									<c:forEach items="${shellFileMap}" var="shellFiles">
										<div class="perinfo_close">
										<h4>${shellFiles.key }</h4>
											<ul>
												<c:forEach items="${shellFiles.value}" var="shellFile">
			        								<li title = "${shellFile.csaID }"><a href="javascript:void(0)">${shellFile.fileName }</a></li>
												</c:forEach>
											</ul>
										</div>
									</c:forEach>
							</div>
	                        <div class="perinfo_l fr" id="shl_tar">
	                        	<ul>
	                            </ul>
	                        </div>
	                    </div>
	               
	                </div>
						<div class="baseinfo" id="note_div">
							<ul>
	                        	<li><b><spring:message code="UpdateNote"></spring:message></b>
		                        	<div class="infoli">
			                        	<textarea id="note_inp" name="note_inp" placeholder="<spring:message code="noteFormat"></spring:message>"></textarea>
									</div>
								</li>
							</ul>
						</div>
	            </div>
	        </div>
	    	<div class="casbtn"><button type="submit" class="btn btn-primary" name="signup1" value="Sign up"><spring:message code="submit"></spring:message></button></div>
	    </div>
    </form>
</div>

	<!--popdiv1-->
	<div id="os3_ipfp" style="display:none">
		<div class="popdiv-mask">
			<div class="popdiv">
				<h2><spring:message code="disParInf"></spring:message>
				<a class="ico_close" href="javascript:void(0)"
					onclick="closePop(this)"></a></h2>
				<div class="popdiv_cnt">
					<ul class="ipfp" id="os_ul">
						<li><b> <spring:message code="RAID"></spring:message></b>
							<div class="ipfpcn">
								<input class="inptext" type="text" placeholder="<spring:message code="raidFormat"></spring:message>" id="raid_inp"  />
							</div>
						</li>
						<li><b> <spring:message code="sysType"></spring:message></b>
							<div class="ipfpcn">
								<select id="sys_inp" class="form-control" onchange= "chDisType(this)">
 										<!-- <option value="PPC PRep Boot">PPC PRep</option> -->
				  						<option value="ext2">ext2</option>
				  						<option value="ext3">ext3</option>
				  						<option value="ext4">ext4</option>
				  						<option value="vfat">vfat</option>
				  						<option value="swap"><spring:message code="swap"></spring:message></option>
				  						<%-- <option value="raid"><spring:message code="softraid"></spring:message></option> --%>
                					</select>
							</div>
						</li>
						<li><b> <spring:message code="size"></spring:message></b>
							<div class="ipfpcn">
								<input class="inptext" id="size_inp" type="text" placeholder="<spring:message code="sizeFormat"></spring:message>" id="size_inp" value="" />
							</div>
						</li>
						
						<!-- <li class="pop_error">错误提示</li> -->
					</ul>
				</div>
				<div class="popdiv_btn">
					<a class="btnblue bodrad5" href="javascript:void(0)"
						onclick="check_save()"><spring:message code="submit"></spring:message></a>
				</div>
			</div>
		</div>
	
	</div>


	<!--popdiv2-->
	<div id="os_copy" style="display:none">
		<div class="popdiv-mask">
			<div class="popdiv">
				<h2><spring:message code="copyKs"></spring:message>
				<a class="ico_close" href="javascript:void(0)"
					onclick="closePop2(this)"></a></h2>
				<div class="popdiv_cnt">
					<ul class="ipfp" id="cp_os_ul">

						<li><b> <spring:message code="ConfName"></spring:message></b>
							<div class="ipfpcn">
								<input  class="inptext" id="copy_conf_name" type="text" value="" placeholder="<spring:message code='ksSearchTip'></spring:message>">
							</div>
						</li>
						<li><b> <spring:message code="osname"></spring:message></b>
							<div class="ipfpcn">
								<input id="copy_iso_name"  class="inptext" type="text" value="" placeholder="<spring:message code='osNull'></spring:message>">
							</div>
						</li>
						<!-- <li class="pop_error">错误提示</li> -->
					</ul>
				</div>
				<div class="popdiv_btn">
					<a class="btnblue bodrad5" href="javascript:void(0)"
						onclick="cp_check_save()"><spring:message code="submit"></spring:message></a>
				</div>
			</div>
		</div>
	
	</div>
	<!--footer-->
	<div class="cas_foot"><span class="mr30">版权所有 © 2016</span>神州泰岳 UltraPower</div>
<script type="text/javascript">
	var str = '${osList}';
	str = str.substring(1, str.length-1);
	var osList = str.split(",");
	$.validator.setDefaults( {
		submitHandler: function () {
			var resutlType = saveToServer();
			if(resutlType.result){
				window.location.href = "osdeploy";
			}else{
				swal("Message!",resutlType.errorMsg);
				return false;
			}
		}
	} );
	
	
	var createTable = function(data){
		var table = $('#os_table');
		table.empty();
		var thead = '<tr><th scope="col"><input type="checkbox" id="allCheck" value="" onclick="selectCheckBox(this)" /> <spring:message code="ConfigName"></spring:message></th><th scope="col"><spring:message code="IsoName"></spring:message></th><th scope="col"><spring:message code="Creator"></spring:message></th><th scope="col"><spring:message code="CreationTime"></spring:message></th><th scope="col"><spring:message code="Remark"></spring:message></th></tr>';
		table.append(thead);
		$.each(data, function(name, value) {
			table.append('<tr><td><input type="checkbox" id="'+this.csaId+'" value="${kickstart.ksName }" onclick="selectCheckBox(this)" />'+this.ksName+'</td><td>'+this.iso+'</td><td>'+this.userAccount+'</td><td>'+this.createTime+'</td><td><a class="bodrad5 edit" href="javascript:void(0)" onclick="viewDetail(this)"><spring:message code="updateHistory"></spring:message></a></td></tr>');
		});
	};
	
	/**
	 * 点击添加按钮事件
	 * 
	 */
	function check_save() {
		var opType = $('#optionType').val();
		
		var raid_inp = $('#raid_inp').val().replace(/(^\s*)|(\s*$)/g, "");
		var sys_inp = $('#sys_inp').find("option:selected").text();
		var selecVal = $('#sys_inp').val();
		var size_inp = $('#size_inp').val().replace(/(^\s*)|(\s*$)/g, "");
		$('#errorLi').remove();
		
		var checkType = true;
		if(selecVal != 'swap'){
			if (raid_inp == "" || raid_inp.substring(0,1) != '/') {
				$('#os_ul').append('<li id="errorLi" class="pop_error"><spring:message code="discNull"></spring:message></li>');
				checkType = false;	
			}
		}
		
		if(!checkType)
			return false;
		
		
		if (size_inp == "" || isNaN(size_inp) || parseFloat(size_inp)<=0) {
			$('#os_ul').append('<li id="errorLi" class="pop_error"><spring:message code="sizeNum"></spring:message></li>');
			checkType = false;
		}else {
			var isFormat = validateStr(raid_inp, "disc_tb",1,opType);
			if (isFormat) {
				$('#os_ul').append('<li id="errorLi" class="pop_error"><spring:message code="existMount"></spring:message></li>');
				checkType = false;
			}
		}
		if(!checkType)
			return false;
		
		
		if(opType == "add"){
//			var tr = '<tr><td>'+raid_inp+'</td><td>'+sys_inp+'</td><td>'+size_inp+'</td><td><a href="javascript:void(0)" onclick="removeSystemDis(this)">编辑</a><a href="javascript:void(0)" onclick="removeSystemDis(this)">删除</a></td></tr>';
			$('#disc_tb').append('<tr><td>'+raid_inp+'</td><td>'+sys_inp+'</td><td>'+size_inp+'</td><td><a href="javascript:void(0)" onclick="editSystemArea(this)"><spring:message code="update"></spring:message></a><a href="javascript:void(0)" onclick="removeSystemDis(this)"><spring:message code="delete"></spring:message></a></td></tr>');
		}else{
			var trIndex = $('#trIndex').val();
			var tr = $('#disc_tb tr').eq(trIndex);
			tr.find('td:first').text(raid_inp);
			tr.find('td').eq(1).text(sys_inp);
			tr.find('td').eq(2).text(size_inp);
		}
		$('#os3_ipfp').hide("slow");
		$('#errorLi').remove();
//		return {'checkType':checkType,'raid_inp':raid_inp,'sys_inp':sys_inp,'size_inp':size_inp,'opType':opType};
	};
	function fillI18n(count){
		$("#selectedlen").text('<spring:message code="selectedNum" arguments="'+count+'"></spring:message>');
	};
	//保存复制kicksstart
	function cp_check_save(){
		$('#cp_os_ul li[class="pop_error"]').remove();
		
		var copy_conf_name = $('#copy_conf_name').val().replace(/(^\s*)|(\s*$)/g, "");
		if(copy_conf_name == ""){
			$('#cp_os_ul').append('<li  class="pop_error"><spring:message code="ksSearchTip"></spring:message></li>');
			return false;
		}else{
			var isFormat = validateConfig(copy_conf_name, "os_table");
			if(isFormat){
				$('#cp_os_ul').append('<li  class="ksSearchTip">c</li>');
				return false;
			}
		}
		
		var copy_iso_name = $('#copy_iso_name').val().replace(/(^\s*)|(\s*$)/g, "");
		if(copy_iso_name == ""){
			$('#cp_os_ul').append('<li  class="pop_error"><spring:message code="osNull"></spring:message></li>');
			return false;
		}else{
			var isFormat = validateIOS(copy_iso_name, osList);
			if(!isFormat){
				$('#cp_os_ul').append('<li  class="pop_error"><spring:message code="osFormat"></spring:message></li>');
				return false;
			}
		}
		var csaId = $('#csaId').val().replace(/(^\s*)|(\s*$)/g, "");
		var data = {'csaId':csaId,'iso_name':copy_iso_name,'conf_name':copy_conf_name};
		var resultData = ajaxWithServer(null,data,"copyKickstart");
		if(resultData.result){
			clearFill();
			window.location.href = "osdeploy";
		}else{
			swal("Message!",resultData.errorMsg);
		}
	}
	
	function fillKisckstart(data){
		$('#conf_name').val(data.conf_name);
		$('#iso_name').val(data.iso_name);
		$('#lan_in').val(data.langue);
		
		$('#time_zone').val(data.timezone);
		$('#root_pwd').val(data.rootpwd);
		$('#root_pwd_confirm').val(data.rootpwd);
		var reboot = data.reboot;
		if(reboot == "reboot"){
			$('#reboot_ch').attr("checked", true);
		}
		var  disc_tb = $('#disc_tb');
		disc_tb.empty();
		
		
		
		disc_tb.append('<tr><th scope="col" width="30%"><spring:message code="RAID"></spring:message></th><th scope="col" width="30%"><spring:message code="sysType"></spring:message></th><th scope="col" width="20%"><spring:message code="size"></spring:message></th><th scope="col" width="20%"><spring:message code="option"></spring:message></th></tr>');
		var partitions = data.partitions;
		$.each(partitions,function(){
			disc_tb.append('<tr><td>'+this.raid+'</td><td>'+this.type+'</td><td>'+this.size+'</td><td><a class="tdedit" href="javascript:void(0)" onclick="editSystemArea(this)"><spring:message code="update"></spring:message></a><a class="tddel" href="javascript:void(0)" onclick="removeSystemDis()"><spring:message code="delete"></spring:message></a></td></tr>');
		});
		
		var packages_tar = $('#packages_tar ul');
		packages_tar.empty();
		var packages = data.packages;
		$.each(packages,function(){
			var li = $('<li title = "'+this.csaId+'"><a href="javascript:void(0)">'+this.packageName +'</a></li>');
			packages_tar.append(li);
			$('#packages_sou div ul li[title="'+this.csaId+'"]').addClass('current');
			li.on('click',function(){
				var tittle = $(this).attr("title");
				$('#packages_sou div ul li').each(function(i,data){
					var tempTitle = $(data).attr('title');
					if(tittle == tempTitle){
						$(data).removeClass();
						return false;
					}
				});
				$(this).remove();
			});
		});
		
		
		var shl_tar = $('#shl_tar ul');
		shl_tar.empty();
		var posts = data.post;
		$.each(posts,function(){
			var li = $('<li title = "'+this.csaID+'"><a href="javascript:void(0)">'+this.fileName +'</a></li>');
			shl_tar.append(li);
			$('#shl_sou div ul li[title="'+this.csaID+'"]').addClass('current');
			li.on('click',function(){
				var tittle = $(this).attr("title");
				$('#shl_sou div ul li').each(function(i,data){
					var tempTitle = $(data).attr('title');
					if(tittle == tempTitle){
						$(data).removeClass();
						return false;
					}
				});
				$(this).remove();
			});
		});
		
	};
	
	function getConfiError1 (){
		return "<spring:message code='ksSearchTip'></spring:message>";
	}
	function getConfiError2 (){
		return "<spring:message code='confexist'></spring:message>";
	}
	function getConfiError3 (){
		return "<spring:message code='osNull'></spring:message>";
	}
	function getConfiError4 (){
		return "<spring:message code='osFormat'></spring:message>";
	}
	function getConfiError5 (){
		return "<spring:message code='confpwd'></spring:message>";
	}
	function getConfiError6 (){
		return "<spring:message code='noteErrorMsg1'></spring:message>";
	}
	function getSpringMsg(code,count){
			if(code =='ConfirmDeleteIps'){
				return '<spring:message code='ConfirmDeleteIps' arguments="'+count+'"></spring:message>';
			}else if(code =='Delete'){
				return '<spring:message code='Delete'></spring:message>';
			}else if(code =="okdelete"){
				return '<spring:message code='okdelete'></spring:message>';
			}else if(code =='cancel'){
				return '<spring:message code='cancel'></spring:message>';
			}else{
				return "confirm delete this data?";
			}
	}
</script>
</body>
</html>
