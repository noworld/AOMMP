<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CSA</title>
<link href="static/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="static/css/sweetalert.css" rel="stylesheet" type="text/css"/>
<link href="static/css/base.css" rel="stylesheet" type="text/css" />
<link href="static/css/main.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	.disableClick{ 
		pointer-events:none;
		cursor:default
	} 
</style>
</head>
<body>
	<jsp:include page="../include/nav.jsp">
		<jsp:param value="deploy" name="menu" />
		<jsp:param value="installProgram" name="menusub" />
	</jsp:include>
<!--content-->
<div class="content" id="table">
	<!--tab&btn&search-->
	<div class="btn_search">
    	<div class="csa_tab bodrad5 bg_white"><a status="1" class="current" href="#unpublished">未发布</a><a status="2" href="#published">已发布</a><a status="3" href="#obsolete">已作废</a><a status="4" href="#executed">已执行</a></div>
    	<div class="csa_btn" id="add_os1" style="visibility:hidden;"><a id="add" href="#">添加</a></div>
    </div>
    <!---->
    <div class="tab-content">
    	<div role="tabpanel" class="tab-pane fade in active" id="unpublished">
		  	<div class="csa_list bodrad5 bg_white">
		        <div class="csa_table">
		        	<div id="unpublishedTabletop" class="csa_tabletop" style="visibility:hidden;margin-left:40px;"> 已选 3 个文件<a id="issueButton" class="bodrad5 edit" href="javascript:void(0);">发布</a><a id="unpublishedDeleteButton" class="bodrad5 edit" href="javascript:void(0);">删除</a><a id="unpublishedUpdateButton" class="bodrad5" href="javascript:void(0);">修改</a></div>
		        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		            </table>
		        </div>
		        <div class="alert alert-info" role="alert" style="margin-bottom: 0px;"><strong>提示!</strong>没有相关数据</div>
		    </div>
	  	</div>
	    <div role="tabpanel" class="tab-pane fade" id="published">
		    <div class="csa_list bodrad5 bg_white">
		        <div class="csa_table">
		        	<div id="publishedTabletop" class="csa_tabletop" style="visibility:hidden;margin-left:40px;"> 已选3个文件<a id="installButton" class="bodrad5 edit" href="javascript:void(0);">安装</a><a id="invalidButton" class="bodrad5 edit" href="javascript:void(0);">作废</a></div>
		        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		            </table>
		        </div>
		        <div class="alert alert-info" role="alert" style="margin-bottom: 0px;"><strong>提示!</strong>没有相关数据</div>
		    </div>
	    </div>
	  	<div role="tabpanel" class="tab-pane fade" id="obsolete">
		  	<div class="csa_list bodrad5 bg_white">
		        <div class="csa_table">
		        	<div id="obsoleteTabletop" class="csa_tabletop" style="visibility:hidden;margin-left:40px;"> 已选 3 个文件<a id="restoreButton" class="bodrad5 edit" href="javascript:void(0);">恢复</a><a id="obsoleteDeleteButton" class="bodrad5 edit" href="javascript:void(0);">删除</a><a id="obsoleteUpdateButton" class="bodrad5" href="javascript:void(0);">修改</a></div>
		        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		            </table>
		        </div>
		        <div class="alert alert-info" role="alert" style="margin-bottom: 0px;"><strong>提示!</strong>没有相关数据</div>
		    </div>
	  	</div>
	  	<div role="tabpanel" class="tab-pane fade" id="executed">
		    <div class="csa_list bodrad5 bg_white">
		        <div class="csa_table">
		        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		            </table>
		        </div>
		        <div class="alert alert-info" role="alert" style="margin-bottom: 0px;"><strong>提示!</strong>没有相关数据</div>
		    </div>
	    </div>
    </div>
</div>

<!--安装明细-->
<div class="content home_os" id="installDetail" style="display:none;margin:50px;">
	<h2><em></em>:安装明细</h2>
    <!---->
    <div class="csa_list bodrad5 bg_white">
        <div class="csa_table">
        	<div class="csa_tabletop" style="display:none"><input type="checkbox" value="" /> 已选 3 个文件<a class="bodrad5 edit" href="javascript:void(0);">安装</a><a class="bodrad5 edit" href="javascript:void(0);">作废</a></div>
        	<table id="installDetails" width="100%" border="0" cellspacing="0" cellpadding="0">
            </table>
        </div>
    </div>
    <div class="casbtn"><a class=" bluebtn200" href="JavaScript:void(0)">返回</a></div>
</div>

<!--content-->
<div class="home_os" id="form" style="display:none">
	<h2>添加安装方案</h2>
	<form id="installProgramForm">
	<input name="csaId" type="hidden" value="">
    <div class="home_oscnt clearfix">
    	<ul class="osbssearch">
            <li class="error"><b>方案名称</b><input name="schemeName" class="osbsinp" type="text" value="" placeholder=""></li>
            <li class="error"><b>OS部署配置</b><label class="datemod"><input type="text" value="" placeholder="" disabled="disabled"><input type="hidden" name="ksCsaId"><s class="down"></s></label>
		        <div class="menu2" id="diqucnt" style="display:none;left:80px;width:300px;">
		        <s class="up"></s>
		        <ul class=" bodrad5">
		        </ul>
		        </div>
	        </li>
        </ul>
        <div class="oscnt fl">
            <div  class="oscntab">
                <ul>
                    <li class="current" title="tab1">应用范围</li>
                </ul>
            </div>
            <div class="bodrad5 bg_white oscntabc">
            	<div id="tab1">
                	<div class="oscntabc_1">
                    </div>
                </div>
            </div>
        </div>
        <div class="oscnt fr">
        	<h3></h3>
            <div class="bodrad5 bg_white oscnt_r ">
            	<ul>
                </ul>
            </div>
        </div>
    </div>
    </form>
    <div class="casbtn"><a id="submitForm" href="###">确定</a><a id="cancelAdd" class="whitebtn" href="###">取消</a></div>
</div>
<div class="modal fade bs-example-modal-lg" id="logModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<!--footer-->
<div class="cas_foot"><span class="mr30">版权所有 © 2016</span>神州泰岳 UltraPower</div>

<script type="text/javascript" src="static/js/jquery-2.2.1.min.js"></script>
<script src="static/js/sweetalert.min.js"></script>
<script src="static/js/jquery.form.min.js"></script>
<script src="static/js/jquery.validate.js"></script>
<script src="static/js/additional-methods.js"></script>
<script type="text/javascript" src="static/css/bootstrap/js/bootstrap.js" ></script>
<script type="text/javascript" src="static/js/extendPagination.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#submitForm').click(function() {
			if(JqValidate()){
				var url = "saveInstallProgramInfo";
				if($('input[name="csaId"]').val()!=null&&$('input[name="csaId"]').val()!=''){
					url = "updateInstallProgramInfo";
				}
				$("#installProgramForm").ajaxSubmit({
				     type: "post",
				     url: url,
				     dataType: "json",
				     success: function(result){
				         if(result!=null){
				        	 $('#form').hide();
			    			 $('#table').show();
					    	 swal("操作成功!", "", "success");
					    	 createTable($('.csa_tab.bodrad5.bg_white a.current').attr('status'),$('.csa_tab.bodrad5.bg_white a.current').attr('href'));
					    	 resetForm();
				         }
				     }
				 });
		    }
		});
		$('#installDetails').on('click','a', function () {
			$.ajax({  
		        type:'POST',
		        dataType:'html',
		        url:'getInstallLog',
		        data:{
	                "logName":$(this).attr("ipAddress"),
	            },
		        success:function(data){
		        	if(data=='FileNotFound'){
		        		swal("找不到日志!", "", "warning");
		        	}else{
		        		$('#logModal').modal({
		    			});
		        		$('#logModal .modal-content').html(data);
		        	}
		        }
		    });
		});
		$('#table').on("click","tr td:last-child a",function(){
			$.ajax({  
		        type:'POST',
		        dataType:'json',
		        url:'getAllInstallDetails',
		        data:{
	                "schemeIp":$(this).attr('schemeId'),
	            },
		        success:function(data){
		        	var html = [];
	        		html.push('<tr><th width="100" scope="col">装机IP</th><th scope="col">所属ISO镜像库</th><th scope="col">状态</th><th scope="col">开始时间</th><th scope="col">备注</th></tr>');
		        	$.each(data, function(i) {
		        		html.push('<tr>');
		        		html.push('<td>' + this.ipAddress + '</td>');
		        		html.push('<td>' + this.server.ip + '</td>');
		        		if(this.ipStatus=='01'){
		                	html.push('<td>未安装</td>');
		                }else if(this.ipStatus=='02'){
		                	html.push('<td>安装中</td>');
		                }else if(this.ipStatus=='03'){
		                	html.push('<td>安装失败</td>');
		                }else if(this.ipStatus=='04'){
		                	html.push('<td>已安装</td>');
		                }
		        		html.push('<td>'+ format(this.createTime, 'yyyy-MM-dd HH:mm:ss') +'</td>');
		                html.push('<td><a href="###" ipAddress="'+ this.ipAddress +'">安装日志</a></td>');
		        		html.push('</tr>');
		        	});
		        	var mainObj = $('#installDetails');
		            mainObj.empty();
		            mainObj.html(html.join(''));
		        }
		    });
			$('#installDetail h2 em').text($(this).parent().parent().find('td:first a').text());
			$('#table').hide();
			$('#installDetail').show();
		});
		$('#installDetail .bluebtn200').click(function(){
			$('#table').show();
			$('#installDetail').hide();
		});
		$('#diqucnt').on("click","a",function(){
			$('.datemod input[type="text"]').val($(this).text());
			$('.datemod input[type="hidden"]').val($(this).attr('csaid'));
			$('#diqucnt').css('display','none');
		});
		$('.datemod').click(function(){
			if($('#diqucnt').css('display')=='none'){
				$('#diqucnt').css('display','block');
			}else if($('#diqucnt').css('display')=='block'){
				$('#diqucnt').css('display','none');
			}
		});
		//点击应用范围内ip添加到右侧已选列表
		$('.bodrad5.bg_white.oscntabc').on("click","a",function(){
			$(this).parent().addClass('current');
			var text = $(this).text();
			var ipId = $(this).attr('csaid');
			var boolean = true;
			$('.bodrad5.bg_white.oscnt_r ul li').each(function(){
			    if($(this).text()==text){
			    	boolean=false;
			    }
			});
			if(boolean){
				$('.bodrad5.bg_white.oscnt_r ul').append('<li class="current"><input name="ipGroup" type="hidden" value="'+ipId+'"><a href="###">'+text+'<s></s></a></li>');
			}
			$('.oscnt.fr h3').html('已选IP数量: '+$('.bodrad5.bg_white.oscnt_r ul li').length);
		});
		//展开折叠应用范围列表
		$('.oscntabc_1').on("click","h4",function(){
			if($(this).parent().attr('class')=='oscntabc_open'){
				$(this).parent().attr('class','oscntabc_close');
			}else if($(this).parent().attr('class')=='oscntabc_close'){
				$(this).parent().attr('class','oscntabc_open');
			}
		});
		/* $('.bodrad5.bg_white.oscntabc').on("mouseover mouseout","li",function(event){
			 if(event.type == "mouseover"){
				 $(this).addClass('current');
			 }else if(event.type == "mouseout"){
				 $(this).removeClass('current');
			 }
		}) */
		/* $('.oscnt.fr').on("mouseover mouseout","li",function(event){
			 if(event.type == "mouseover"){
				 $(this).addClass('current');
			 }else if(event.type == "mouseout"){
				 $(this).removeClass('current');
			 }
		}) */
		//取消已选ip
		$('.oscnt.fr').on("click","s",function(){
			var csaid = $(this).parent().prev().val();
			$('a[csaid='+csaid+']').parent().removeClass('current');
			$(this).parent().parent().remove();
			$('.oscnt.fr h3').html('已选IP数量: '+$('.bodrad5.bg_white.oscnt_r ul li').length);
		});
		//添加方案
		$('#add').click(function(){
			$.ajax({
		        type:'POST',
		        dataType:'json',
		        url:'getAllKs',
		        success:function(data){
		        	$.each(data.kickStartList, function(i) {
		        		$('#diqucnt ul').append('<li><a csaid="'+this.csaId+'" href="###">'+this.ksName+'</a></li>');
		        	});
		        	var groupNum = 1;
		        	$.each(data.ipList, function(j) {
		        		if($('#batChid_'+this.batChid).length>0){
		        			$('#batChid_'+this.batChid+' ul').append('<li><a csaid="'+this.csaId+'" href="###">'+this.ipAddress+'</a></li>');
		        		}else{
		        			if(groupNum!=1){
			        			$('#tab1 .oscntabc_1').append('<div id="batChid_'+this.batChid+'" class="oscntabc_close"><h4>'+this.batChid+'</h4><ul><li><a csaid="'+this.csaId+'" href="###">'+this.ipAddress+'</a></li></ul></div>');
		        			}else{
			        			$('#tab1 .oscntabc_1').append('<div id="batChid_'+this.batChid+'" class="oscntabc_open"><h4>'+this.batChid+'</h4><ul><li><a csaid="'+this.csaId+'" href="###">'+this.ipAddress+'</a></li></ul></div>');
		        			}
		        			groupNum = groupNum+1;
		        		}
		        	});
		        }
		        });
			$('.oscnt.fr h3').html('已选IP数量: '+$('.bodrad5.bg_white.oscnt_r ul li').length);
			$('#table').hide();
			$('#form').show();
		});
		//修改方案
		$('#unpublishedUpdateButton,#obsoleteUpdateButton').click(function(){
			if($('input[name="csaId"]')!=null&&$('input[name="csaId"]')!=''){
				$.ajax({
			        type:'POST',
			        dataType:'json',
			        url:'getAllKs',
			        data:{"csaId":$('input[name="csaId"]').val()},
			        success:function(data){
			        	$.each(data.kickStartList, function(i) {
			        		$('#diqucnt ul').append('<li><a csaid="'+this.csaId+'" href="###">'+this.ksName+'</a></li>');
			        	});
			        	$('input[name="schemeName"]').val(data.installProgram.schemeName);
			        	$('.datemod input[type="text"]').val($('a[csaid="'+data.installProgram.ksCsaId+'"]').text());
						$('.datemod input[type="hidden"]').val(data.installProgram.ksCsaId);
			        	var groupNum = 1;
			        	$.each(data.ipList, function(j) {
			        		if($('#batChid_'+this.batChid).length>0){
			        			$('#batChid_'+this.batChid+' ul').append('<li><a csaid="'+this.csaId+'" href="###">'+this.ipAddress+'</a></li>');
			        		}else{
			        			if(groupNum!=1){
				        			$('#tab1 .oscntabc_1').append('<div id="batChid_'+this.batChid+'" class="oscntabc_close"><h4>'+this.batChid+'</h4><ul><li><a csaid="'+this.csaId+'" href="###">'+this.ipAddress+'</a></li></ul></div>');
			        			}else{
				        			$('#tab1 .oscntabc_1').append('<div id="batChid_'+this.batChid+'" class="oscntabc_open"><h4>'+this.batChid+'</h4><ul><li><a csaid="'+this.csaId+'" href="###">'+this.ipAddress+'</a></li></ul></div>');
			        			}
			        			groupNum = groupNum+1;
			        		}
			        	});
			        	$.each(data.chosenipList, function(n) {
			        		$('.bodrad5.bg_white.oscnt_r ul').append('<li class="current"><input name="ipGroup" type="hidden" value="'+this.csaId+'"><a href="###">'+this.ipAddress+'<s></s></a></li>');
			    			$('a[csaid='+this.csaId+']').parent().addClass('current');
			        	});
			        }
			     });
			}
			$('.oscnt.fr h3').html('已选IP数量: '+$('.bodrad5.bg_white.oscnt_r ul li').length);
			$('#table').hide();
			$('#form').show();
		});
		//取消添加修改方案操作
		$('#cancelAdd').click(function(){
			$('#form').hide();
			$('#table').show();
			resetForm();
		});
		//方案的安装，作废，删除等操作按钮
		$("#installButton").click(function() {
			updateSchemeStatus("确认应用所选方案进行安装吗？","是的，我要安装",4);
        });
		$("#invalidButton").click(function() {
			updateSchemeStatus("确认作废所选方案？","是的，我要作废",3);
        });
		$("#obsoleteDeleteButton").click(function() {
			updateSchemeStatus("确认删除所选方案？","是的，我要删除",0);
        });
		$("#restoreButton").click(function() {
			updateSchemeStatus("确认恢复所选方案？","是的，我要恢复",1);
        });
		$("#issueButton").click(function() {
			updateSchemeStatus("确认发布所选方案？","是的，我要发布",2);
        });
		$("#unpublishedDeleteButton").click(function() {
			updateSchemeStatus("确认删除所选方案？","是的，我要删除",0);
        });
		//应用范围折叠展示
		$(document).on("click",'a[role="button"]', function() {
			var span = $(this).find('span');
			if(span.attr('class')=='glyphicon glyphicon-triangle-right'){
				span.attr('class','glyphicon glyphicon-triangle-bottom');
				$(this).parent().parent().next().css("display","");
			}else{
				span.attr('class','glyphicon glyphicon-triangle-right');
				$(this).parent().parent().next().css("display","none");
			}
		});
		//标签页
		$('.csa_tab a').click(function (e) {
			$('a.current').removeClass('current');
			$(this).addClass('current');
			if($('a.current').attr('href')=='#unpublished'){
				$('.csa_btn').css("visibility","visible");
			}else{
				$('.csa_btn').css("visibility","hidden");
			}
			$(this).tab('show');
		});
		$('.csa_tab a').on('shown.bs.tab', function () {
			createTable($(this).attr('status'),$(this).attr('href'));
		});
	})
	//按钮组动态显示
	$(document).ready(function() {
		createTable('1','#unpublished');
	    $(document).on("click","input[type='checkbox']", function() {
		 	if($(this).attr('name')=='all'){
		 		if($(this).prop('checked')){
		 			$(this).parent().parent().parent().find("input[type='checkbox']").prop("checked",true);
		 		}else{
		 			$(this).parent().parent().parent().find("input[type='checkbox']").prop("checked",false);
		 		}
		 	}
		 	var publishedLen = $("input:checkbox:checked[name='publishedCheckbox']").length;
		 	if(publishedLen<1){
				$("#publishedTabletop").css("visibility","hidden");
				$("#published input[name='all']").prop("checked",false);
			}else{
				if(publishedLen==$("input:checkbox[name='publishedCheckbox']").length){
					$("#published input[name='all']").prop("checked",true);
				}
				$("#publishedTabletop").css("visibility","visible");
				$("#installButton").addClass('edit');
				$("#invalidButton").addClass('edit');
			}
		 	var unpublishedLen = $("input:checkbox:checked[name='unpublishedCheckbox']").length;
		 	if(unpublishedLen<1){
				$("#unpublishedTabletop").css("visibility","hidden");
				$("#unpublished input[name='all']").prop("checked",false);
			}else{
				if(unpublishedLen==$("input:checkbox[name='unpublishedCheckbox']").length){
					$("#unpublished input[name='all']").prop("checked",true);
				}
				$("#unpublishedTabletop").css("visibility","visible");
				if(unpublishedLen>1){
					$("#unpublishedUpdateButton").removeClass('edit').addClass('disableClick');
				}else{
					if(unpublishedLen==1&&$(this).prop('checked')){
						$('input[name="csaId"]').val($(this).attr('schemeId'));
					}
					$("#issueButton").addClass('edit').removeClass('disableClick');
					$("#unpublishedDeleteButton").addClass('edit').removeClass('disableClick');
					$("#unpublishedUpdateButton").addClass('edit').removeClass('disableClick');
				}
			}
		 	var obsoleteLen = $("input:checkbox:checked[name='obsoleteCheckbox']").length;
		 	if(obsoleteLen<1){
				$("#obsoleteTabletop").css("visibility","hidden");
				$("#obsolete input[name='all']").prop("checked",false);
			}else{
				if(obsoleteLen==$("input:checkbox[name='obsoleteCheckbox']").length){
					$("#obsolete input[name='all']").prop("checked",true);
				}
				$("#obsoleteTabletop").css("visibility","visible");
				if(obsoleteLen>1){
					if(obsoleteLen==1&&$(this).prop('checked')){
						$('input[name="csaId"]').val($(this).attr('schemeId'));
					}
					$("#obsoleteUpdateButton").removeClass('edit').addClass('disableClick');
				}else{
					$("#restoreButton").addClass('edit').removeClass('disableClick');
					$("#obsoleteDeleteButton").addClass('edit').removeClass('disableClick');
					$("#obsoleteUpdateButton").addClass('edit').removeClass('disableClick');
				}
			}
	    })
	})
	//ajax异步请求数据动态创建表格
	function createTable(status,tabId){
		var name = tabId.replace('#', '')+'Checkbox';
		var data = getInstallProgramByStatus(status);
		var installProgramList = data.installProgramList;
		if(installProgramList.length<=0){
			$('.tab-pane.fade.in.active div.csa_table').css("display","none");
			$('.tab-pane.fade.in.active div.alert').css("display","block");
		}else{
			$('.tab-pane.fade.in.active div.alert').css("display","none");
			$('.tab-pane.fade.in.active div.csa_table').css("display","block");
			var html = [];
			if(status=='4'){
		        html.push(' <tr><th width="100" scope="col"> 方案名称</th><th scope="col">OS部署配置</th><th scope="col">创建账号</th><th scope="col">创建时间</th><th scope="col">状态</th><th scope="col">备注</th></tr>');
			}else{
		        html.push(' <tr><th width="100" scope="col"><input name="all" type="checkbox" value="" /> 方案名称</th><th scope="col">OS部署配置</th><th scope="col">创建账号</th><th scope="col">创建时间</th><th scope="col">状态</th></tr>');
			}
	        $.each(installProgramList, function(i) {
	            html.push('<tr>');
	            if(status=='4'){
	                html.push('<td valign="top" nowrap> <a role="button" href="#"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>'+this.schemeName+'</a></td>');
	            }else{
	                html.push('<td valign="top" nowrap><input schemeId="'+this.csaId+'" name="'+name+'" type="checkbox" value="" /> <a role="button" href="#"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>'+this.schemeName+'</a></td>');
	            }
	            html.push('<td nowrap>'+this.kickStart.ksName+'</td>');
	            html.push('<td nowrap>'+this.userAccount+'</td>');
	            html.push('<td nowrap>'+format(this.createTime, 'yyyy-MM-dd HH:mm:ss')+'</td>');
	            if(this.installStatus=='0'){
	            	html.push('<td nowrap>未执行</td>');
	            }else if(this.installStatus=='1'){
	            	html.push('<td nowrap>安装中<button style="margin-left:5px;" type="button" class="btn btn-info btn-xs"> 总数 <span class="badge">4</span><button style="margin-left:5px;" type="button" class="btn btn-warning btn-xs"> 进行中 <span class="badge">2</span></button><button style="margin-left:5px;" type="button" class="btn btn-success btn-xs"> 已成功 <span class="badge">1</span><button style="margin-left:5px;" type="button" class="btn btn-danger btn-xs"> 安装失败 <span class="badge">1</span></td>');
	            	//html.push('<td nowrap>安装中&nbsp;<span title="已完成" style="background-color:#5cb85c;" class="badge">2</span>&nbsp;<span title="进行中" style="background-color:#f0ad4e;" class="badge">4</span>&nbsp;<span title="安装失败" style="background-color:#d9534f;" class="badge">6</span>&nbsp;<span title="总数" style="background-color:#5bc0de;" class="badge">8</span></td>');
	            }else if(this.installStatus=='2'){
	            	html.push('<td nowrap>已完成</td>');
	            }else if(this.installStatus=='3'){
	            	html.push('<td nowrap>已完成有失败</td>');
	            }
	            if(status=='4'){
	            	html.push('<td nowrap><a schemeId="'+this.csaId+'" href="#">安装明细</a></td>');
	            }
	            html.push('</tr>');
	            html.push('<tr style="display:none;">');
	            html.push('<td colspan="6"><div class="divgrey"><em>应用范围:</em>'+ this.applications +'</div></td>');
	            html.push('</tr>');
	        });
	        var mainObj = $(tabId+'').find('table');
	        mainObj.empty();
	        mainObj.html(html.join(''));
		}
	}
	//根据不同状态获取方案列表
	function getInstallProgramByStatus(status){
    	var dataJson = null;
    	$.ajax({
	        type:'POST',
	        async:false,
	        dataType:'json',
	        url:'getInstallPlan',
	        data:{
                "status":status,
            },
	        success:function(data){
	        	dataJson = data;
	        }
	        });
    	return dataJson;
    }
	//时间格式转化
	var format = function(time, format){
        var t = new Date(time);
        var tf = function(i){return (i < 10 ? '0' : '') + i};
        return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a){
            switch(a){
                case 'yyyy':
                    return tf(t.getFullYear());
                    break;
                case 'MM':
                    return tf(t.getMonth() + 1);
                    break;
                case 'mm':
                    return tf(t.getMinutes());
                    break;
                case 'dd':
                    return tf(t.getDate());
                    break;
                case 'HH':
                    return tf(t.getHours());
                    break;
                case 'ss':
                    return tf(t.getSeconds());
                    break;
            }
        })
    }
	//表单校验
	function JqValidate(){
	    return $('#installProgramForm').validate({
	    	rules:{
	    		schemeName:{
                    required:true,
                    checkSchemeName:true
                },
            },
            messages:{
            	schemeName:{
                    required:'必填'
                },
            }
	    }).form();  
	}
	function resetForm(){
		$('#installProgramForm input').each(function(){
		    $(this).val('');
		  });
		$('#diqucnt ul').html('');
		$('#tab1 .oscntabc_1').html('');
		$('.bodrad5.bg_white.oscnt_r ul').html('');
	}
	function updateSchemeStatus(title,buttonText,status){
		swal({
            title: title,
            text: "",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: buttonText,
            confirmButtonColor: "#2a94e4"
        }, function() {
        	var idArr = [];
        	$(".tab-pane.fade.in.active input:checkbox:checked[name!='all']").each(function(){
        	    idArr.push($(this).attr('schemeid'));
        	});
        	$.ajax({
		        type:'POST',
		        dataType:'json',
		        url:'updateSchemeStatus',
		        data:{'idArr[]':idArr,'status':status},
		        success:function(data){
		        	swal("操作成功!", "", "success");
		        	$('.tab-pane.fade.in.active .csa_tabletop').css("visibility","hidden");
		        	createTable($('.csa_tab.bodrad5.bg_white a.current').attr('status'),$('.csa_tab.bodrad5.bg_white a.current').attr('href'));
		        }
		  });
    	  });
	}
    var format = function(time, format){
        var t = new Date(time);
        var tf = function(i){return (i < 10 ? '0' : '') + i};
        return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a){
            switch(a){
                case 'yyyy':
                    return tf(t.getFullYear());
                    break;
                case 'MM':
                    return tf(t.getMonth() + 1);
                    break;
                case 'mm':
                    return tf(t.getMinutes());
                    break;
                case 'dd':
                    return tf(t.getDate());
                    break;
                case 'HH':
                    return tf(t.getHours());
                    break;
                case 'ss':
                    return tf(t.getSeconds());
                    break;
            }
        })
    }
</script>
</body>
</html>
