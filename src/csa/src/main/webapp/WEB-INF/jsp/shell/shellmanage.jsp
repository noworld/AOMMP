<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CSA</title>
		<script type="text/javascript" src="static/js/jquery-2.2.1.min.js" ></script>
		<script type="text/javascript" src="static/js/jquery.form.min.js" ></script>
        <script type="text/javascript" src="static/js/extendPagination.js"></script>
		<script src="static/js/search.js" type="text/javascript"></script>
		<script src="static/js/shell.js" type="text/javascript"></script>
		<script src="static/js/sweetalert.min.js" type="text/javascript"></script>
		<script src="static/js/sweetalert-dev.js" type="text/javascript"></script>
		<script src="static/css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<link href="static/css/bootstrap/css/bootstrap.css" type="text/css" />
		<link href="static/css/bootstrap/css/bootstrap.min.css" rel="stylesheet"  type="text/css" />
		<link rel="stylesheet" type="text/css" href="static/css/sweetalert.css"/>
		<link href="static/css/base.css" rel="stylesheet" type="text/css" />
		<link href="static/css/main.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
    
    <jsp:include page="../include/nav.jsp">
		<jsp:param value="library" name="menu"/>
		<jsp:param value="shellview" name="menusub"/>
	</jsp:include>
	
	
	<!--content-->
<div class="content">
	<!--tab&btn&search-->
	<div class="btn_search">
    	<div class="csa_btn" id="upload_ios1"><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal"><spring:message code="uploadShellFile"></spring:message></a></div>
        <div class="csa_search bodrad5 bg_white"><input id="fileName_search" type="text" value="" placeholder="<spring:message code="inputshellname" ></spring:message>" /><a id="quick_search_but"  href="javascript:void(0);" ></a></div>
    </div>
    <!---->
    <div class="csa_list bodrad5 bg_white">
        <div id="tabList"  class="csa_tab80 bodrad5 bg_white">
        		<c:forEach items="${shellTypeList}" var="shellType"  varStatus="status">
        			<c:if test="${status.index == 0}">
        				<a class="current"  href="javascript:void(0);" >${shellType}</a>
        			</c:if>
        			<c:if test="${status.index >0}">
        				<a href="javascript:void(0);" >${shellType}</a>
        			</c:if>
        		</c:forEach>
        	</div>
        
        <div class="csa_table">
        	<div class="csa_tabletop" style="visibility:hidden;">
        	    <input id="allSelected"  type="checkbox" value="" />
        	    <span id="selectedlen"><spring:message code="selectedNum" arguments="3"></spring:message></span>
        		<a id="deleteBut" class="bodrad5 edit" href="javascript:void(0);"><spring:message code="Delete"></spring:message></a>
        		<!--<a class="bodrad5" href="javascript:void(0);">修改</a>  -->
        	</div>
        	<table id="shellTable" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th scope="col"><input id="1-checkbox"  type="checkbox" value="" /><spring:message code="FileName"></spring:message></th>
                <th scope="col"><spring:message code="FileSize"></spring:message></th>
                <th scope="col"><spring:message code="uploadUser"></spring:message></th>
                <th scope="col"><spring:message code="uploadTime"></spring:message></th>
              </tr>
            </table>
        </div>

</div>
    
        <!-- Modal -->
 	<div class="modal fade" id="myModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
      			<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title" id="myModalLabel"><spring:message code="SelectShell"></spring:message></h4>
      			</div>
      			<div class="modal-body">
      			
      				<form id="shellfileupload" class="form-horizontal" method="post" enctype="multipart/form-data"  accept-charset="UTF-8">
      				<input id="fileid"  name="file" type="file" style="display: none" onchange="setFileDisplayName()"  accept=".sh,.txt,.yml"  />
      				<div class="form-group">
   							 <label class="col-sm-2 control-label"><spring:message code="ShellFile"></spring:message></label>
    						<div class="col-sm-10">
   										 <div class="input-group">
      											<input id="fileName" type="text" class="form-control" placeholder="">
      											<span class="input-group-btn">
       												<button  class="btn btn-default" type="button" onclick="selectFile()"><spring:message code="SelectFile"></spring:message></button>
      											</span>
    									</div>
    						</div>
  						</div>
					  	<div class="form-group">
					    	<label  class="col-sm-2 control-label"><spring:message code="ShellType"></spring:message></label>
					    	<div class="col-sm-10">
					      		<select id="filletype" class="form-control">
 										<option><spring:message code="deploy"></spring:message></option>
				  						<option><spring:message code="install"></spring:message></option>
				 						<option><spring:message code="Polling"></spring:message></option>
				  						<option><spring:message code="Collection"></spring:message></option>
                					</select>
					   		</div>
					  	</div>
					</form>
      		</div>
     	 <div class="modal-footer" style="text-align:center">
        	<button type="button" class="btn btn-primary"  onclick="filesubmit()"><spring:message code="Confirm"></spring:message></button>
        	<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="Close"></spring:message></button>
      	</div>
    </div>
  </div>
</div>
    


	<script type="text/javascript">
	//为标签组添加事件
	$(document).ready(function(){
		$('.csa_list').on("click", ".csa_tab80.bodrad5.bg_white a",function(){
		//$('.csa_tab80.bodrad5.bg_white a').on('click',function(){
			$('a.current').removeClass('current');
			$(this).addClass('current');
			//标签切换时需要清除表格选中状态和工具栏显示
			$("input[type='checkbox']").prop("checked",false);
			$(".csa_tabletop").css("visibility","hidden");
			refreshTable($(this).text());
		});
		
		//标签页切换时间
		var a=$(".csa_tab80.bodrad5.bg_white a[class='current']")
		if(a==undefined)
			refreshTable("null");
		else{
			var fileType=a.text();
			refreshTable(fileType);
		}
	});
		
	//删除按钮事件
	$("#deleteBut").on("click",function(){
		deleteFile();
	});
	
	//按钮组动态显示
	$(document).ready(function() {
	    var lastDom = null;
	   $('.csa_table').on("click", "input[type='checkbox']",function() {
	  //  $("input[type='checkbox']").on("click", function() {
		 	if($(this).prop('checked') && $(this).attr('id').indexOf('-checkbox')>=0){
		 		$("input[type='checkbox']").prop("checked",true);
		 	}
		 	if($(this).prop('checked')==false && $(this).attr('id')=="allSelected"){
		 		$("input[type='checkbox']").prop("checked",false);
		 	}
		 	if($(this).attr('id').indexOf('-checkbox')<0 && $(this).attr('id')!="allSelected"){
		 		$("#allSelected").prop("checked",false);
		 	}
		 	
			var len = $("input:checkbox:checked[name='flag']").length;
			if(len<1){
				$(".csa_tabletop").css("visibility","hidden");
				$("input[type='checkbox']").prop("checked",false);
			}else{
				if(len==$("input:checkbox[name='flag']").length)
					$("#allSelected").prop("checked",true);
				$("#selectedlen").text('<spring:message code="selectedNum" arguments="'+len+'"></spring:message>');
				$(".csa_tabletop").css("visibility","visible");
				if(len>1){
					$("a[name='Log']").removeClass('edit').addClass('disableClick');
					$("a[name='Update']").removeClass('edit').addClass('disableClick');
				}else{
					$("a[name='Log']").addClass('edit').removeClass('disableClick');
					$("a[name='Update']").addClass('edit').removeClass('disableClick');
				}
			}
	    });

	   //模态框关闭时清空文件名称
	   $('#myModal').on('hidden.bs.modal', function (e) {
			$("#fileName").val("")
	   });
	   
	 //模态框显示时根据tab类型自动设置select
	   $('#myModal').on('show.bs.modal', function (e) {
		   var a=$(".csa_tab80.bodrad5.bg_white a[class='current']")
			if(a==undefined)
				return;
			else{
				var fileType=a.text();
				$("#filletype").val(fileType);
			}
	   });
	   
	});
	msg={
		    inputshellname : '<spring:message code="inputshellname"></spring:message>',
			InuptfileNameFirst  : '<spring:message code="InuptfileNameFirst"></spring:message>',
			FindFileError  :  '<spring:message code="FindFileError"></spring:message>',
			ShellType  :  '<spring:message code="ShellType"></spring:message>',
			FileSize  :  '<spring:message code="FileSize"></spring:message>',
			uploadUser  :  '<spring:message code="uploadUser"></spring:message>',
			uploadTime  :  '<spring:message code="uploadTime"></spring:message>',
			SelectFileupload  :  '<spring:message code="SelectFileupload"></spring:message>',
			uploadFileError  :   '<spring:message code="uploadFileError"></spring:message>',
			FileHasUpload  :   '<spring:message code="FileHasUpload"></spring:message>',
			CheckFileupload  :   '<spring:message code="CheckFileupload"></spring:message>',
			ConfirmDelete  :   '<spring:message code="ConfirmDelete"></spring:message>',
			DeleteError :  '<spring:message code="DeleteError"></spring:message>',
			ConfirmDelete_cancel :  '<spring:message code="cancel"></spring:message>',
			ConfirmDelete_delete :  '<spring:message code="okdelete"></spring:message>'
	};
	   
	</script>
	
	<!--footer-->
<div class="cas_foot"><span class="mr30">版权所有 © 2016</span>神州泰岳 UltraPower</div>
  </body>
</html>
