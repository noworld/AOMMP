package com.ultrapower.ultracsa.product.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContext;

import com.ultrapower.ultracsa.product.pojo.IP;

/**
 * 
 * 作者: GuoPengfei
 * 
 * 日期: 2016 4 23
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public interface IPService {
	/**
	 * 查询当前页的数据
	 * 
	 * @param ip_search
	 *            搜索值
	 * @param currentPage
	 *            当前页
	 * @param rows
	 *            每页行数
	 * @return
	 */
	public Map<String, Object> IPList(String ip_search, int currentPage,
			int rows);

	/**
	 * 新增一个IP
	 * 
	 * @param map
	 *            新增的数据参数值
	 * @param requestContext
	 *            上下文，用于获取国际化信息
	 * @return
	 */
	public Map<String, Object> insertIP(Map<String, String> map,
			RequestContext requestContext);

	/**
	 * 修改一个IP
	 * 
	 * @param map
	 *            要修改的数据参数值
	 * @param requestContext
	 *            上下文，用于获取国际化信息
	 * @return
	 */
	public Map<String, Object> updateIP(Map<String, String> map,
			RequestContext requestContext);

	/**
	 * 批量删除ip
	 * 
	 * @param map
	 *            要删除的ip的主键集合
	 * @return
	 */
	public Map<String, Object> batchDeleteIps(Map<String, String> map);

	/**
	 * 批量新增、修改ip
	 * 
	 * @param userIp
	 *            用户ip
	 * @param file
	 *            excel文件
	 * @param session
	 *            缓存，用于获取用户名称
	 * @param requestContext
	 *            上下文，用户获取国际化信息
	 * @return
	 */
	public Map<String, Object> inserIPs(String userIp, MultipartFile file,
			HttpSession session, RequestContext requestContext);
	/**
	 * 装机明细获取ip列表
	 * @param ip_search
	 * @param currentPage
	 * @param rows
	 * @return
	 */
	public Map<String, Object> IPListByInstallDetail(String ip_search,int currentPage, int rows,String ipstatus);
	public List<IP> IPListByScheme(Integer csaid);

}
