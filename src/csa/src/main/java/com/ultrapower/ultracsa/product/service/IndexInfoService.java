package com.ultrapower.ultracsa.product.service;

import java.util.List;
import java.util.Map;

public interface IndexInfoService {
	public List<Map<String,String>>  findbyIpStatus();
	public List<Map<String,String>>  findbyIsoType();
	public List<Map<String,String>>  findbySchemeSyatus();
	public List<Map<String,String>> findbyBatchId();
	public String findIp(String ipaddress);
}
