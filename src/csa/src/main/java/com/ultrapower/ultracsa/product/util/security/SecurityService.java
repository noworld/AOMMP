package com.ultrapower.ultracsa.product.util.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ultrapower.accredit.common.value.Resource;
import com.ultrapower.accredit.rmiclient.RmiClientApplication;
import com.ultrapower.ultracsa.product.conf.ProjectConfigServer;

/**
 * 
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于初始化CSA权限点，调用PASM API将功能菜单和按钮封装为权限点送到pasm中
 */
public class SecurityService {
	private static Logger log = Logger.getLogger(SecurityService.class);
	
	public static boolean hasPrivilege(String ressourceID,String userAccount){
		if(!"true".equals(ProjectConfigServer.getConfig("core","pasm_integration")))
				return true;
	    if (ressourceID ==null|| ressourceID.equals("")||ressourceID.equals("null")) {
            return false;
        }
	    if (userAccount ==null|| userAccount.equals("")||userAccount.equals("null")) {
            return false;
        }
	    
	    if("root".equals(userAccount) || isAdmin(userAccount))
	        return true;
	    try
        {
	    	Class cla=Class.forName("com.ultrapower.accredit.rmiclient.RmiClientApplication");
	        Map map= RmiClientApplication.getInstance().getSecurityService().getPrivilegeMap(userAccount, "CMDB",ressourceID);
	        if(map!=null && map.containsKey("P001") &&
	                "true".equals(map.get("P001")+""))
	            return true;
        }
        catch(Exception e)
        {
            log.error("", e);
        }
	    return false;
	}

	/**
	 * @param userName
	 * @param operation
	 * @return 判断是否管理员
	 */
	public static boolean isAdmin(String user){
		if(!"true".equals(ProjectConfigServer.getConfig("core","pasm_integration")))
				return true;
		if(user == null || "".equals(user)){
			return false;
		}
		return RmiClientApplication.getInstance().getSecurityService().isAdministrator(user);
	}
	
	/**
	 * 想PASM同步权限点，同步时会先删除现有的权限点，然后重新生成
	 * @return
	 */
	public static boolean initPasmResource(){
		try {
			//删除已有的权限点
			RmiClientApplication.getInstance().getSecurityService().initResourceByApp("CSA");
			
			//获取所有的权限点
			List<Resource> list=new ArrayList();
			Map<String,String> resMap=ProjectConfigServer.getConfig("pasm");
			for (Iterator iterator = resMap.keySet().iterator(); iterator.hasNext();) {
				String resourceID = (String) iterator.next();
				Resource resourceApp = new Resource();
				resourceApp.setResourceId(resourceID);
				String[] names=resMap.get(resourceID).split(":");
				resourceApp.setName(names[0]);
				if(names[1].equals("0"))
					resourceApp.setSuperId("CSA1");
				else
					resourceApp.setSuperId(names[1]);
				resourceApp.setLoadType(0);
				resourceApp.setNote(names[0]);
				resourceApp.setPpType(3);
				resourceApp.setAccessType("validity");
				resourceApp.setAppName("CSA");
				list.add(resourceApp);
			}
			RmiClientApplication.getInstance().addResources(list);
		} catch (Exception e) {
			log.error("",e);
			return false;
		}
		return true;
	}
	
	/**
	 * 用于查询登陆应用后默认显示的页面
	 * @param userAccount
	 * @return   格式为为   标签ID:标签名称:页面请求地址    以半角冒号分隔
	 */
	/*public static  String getFirstMenu(String userAccount){
		Map<String,String> resMap=ProjectConfigServer.getConfig("pasm");
		List<String> list=new ArrayList(resMap.keySet());
		Collections.sort(list);
		for (String resource:list) {
			String value=resMap.get(resource);
			String[] arr=value.split(":");
			if(arr.length==2)
				continue;
			if(hasPrivilege(resource,userAccount))
				return resource+":"+arr[0]+":"+arr[2];
		}
		return null;
		
	}*/
	
	/**
	 * 返回导航栏上可显示的功能菜单
	 * @param userAccount
	 * @return 结果集中的子List第一个元素为一级菜单信息，其他元素为二级菜单信息。菜单信息格式为：  标签ID:标签名称:页面请求地址(有的话)    以半角冒号分隔
	 */
	public static  List<List<String>> getMenuList(String userAccount){
		Map<String,String> resMap=ProjectConfigServer.getConfig("pasm");
		List<String> list=new ArrayList(resMap.keySet());
		Collections.sort(list);
		for(int i=list.size()-1;i>=0;i--){
			if(!hasPrivilege(list.get(i),userAccount))
				list.remove(i);
		}
		List<List<String>> topLevel=new ArrayList();
		List<String> subLevel=null;
		for (int i=0;i<list.size();i++) {
			String value=resMap.get(list.get(i));
			int index = value.lastIndexOf(":");
			if(list.get(i).length()==8){
				subLevel=new ArrayList();
				subLevel.add(value.substring(index+1));
				topLevel.add(subLevel);
			}else{
				subLevel.add(value.substring(index+1));
			}
			/*String[] arr=value.split(":");
			if(arr[1].equals("0")){
				subLevel=new ArrayList();
				subLevel.add(list.get(i)+":"+arr[0]+":"+(arr.length==2?"":arr[2]));
				topLevel.add(subLevel);
			}else{
				subLevel.add(list.get(i)+":"+arr[0]+":"+arr[2]);
			}*/
		}
		return topLevel;
	}
	
	
}
