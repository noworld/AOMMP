/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.util;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于系统内部获取注册到Spring中的bean，方便复用各个模块
 */
public class BeanUtil<T> {
	
	public static <T> T getBean(Class<T> tClass){
		WebApplicationContext  contex=ContextLoader.getCurrentWebApplicationContext();
		return contex.getBean(tClass);
	}

}
