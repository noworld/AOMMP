/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ultrapower.ultracsa.product.conf.ProjectPathCfg;
import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.service.IPService;
import com.ultrapower.ultracsa.product.util.WordToHtml;

/**
 * 作者: guodong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 安装明细
 */
@Controller
public class InstallDetailsController {

	private static Logger logger = Logger.getLogger(InstallDetailsController.class);
	
	@Resource
	private IPService ipService;
	
	@RequestMapping(value = "/installDetails")
	public String installDetails(Model model){
		return"deploy/install_details";
	}
	@RequestMapping(value = "/getInstallDetails", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getInstallDetails(Integer pageNo,Integer pageSize,String ipstatus,String search,Model model){
		Map<String, Object> map = ipService.IPListByInstallDetail(search,pageNo, pageSize,ipstatus);
		return map;
	}
	
	@RequestMapping(value = "/getInstallLog", method = RequestMethod.POST)
	@ResponseBody
	public String getInstallLog(String logName){
		return WordToHtml.txtToString(ProjectPathCfg.getProjectFilePath()+File.separator+"log"+File.separator+logName+".txt");
	}
	
	@RequestMapping(value = "/getAllInstallDetails", method = RequestMethod.POST)
	@ResponseBody
	public List<IP> getAllInstallDetails(Integer schemeIp){
		List<IP> ipList = ipService.IPListByScheme(schemeIp);
		return ipList;
	}

}
