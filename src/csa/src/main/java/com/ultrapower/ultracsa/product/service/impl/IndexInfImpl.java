package com.ultrapower.ultracsa.product.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ultrapower.ultracsa.product.dao.IndexInfoDao;
import com.ultrapower.ultracsa.product.service.IndexInfoService;
@Service("IndexInfService")
public class IndexInfImpl implements IndexInfoService{
	@Resource
	private IndexInfoDao indexInfoDao;
	
	
	@Override
	 public List<Map<String,String>>   findbyIpStatus(){
		return this.indexInfoDao.findbyIpStatus();
		
	}


	@Override
	public List<Map<String,String>>  findbyIsoType() {
		// TODO Auto-generated method stub
		return this.indexInfoDao.findbyIsoType();
	}

 
	@Override
	public List<Map<String,String>>  findbySchemeSyatus() {
		// TODO Auto-generated method stub
		return this.indexInfoDao.findbySchemeSyatus();
	}


	@Override
	public List<Map<String,String>> findbyBatchId() {
		// TODO Auto-generated method stub
		return this.indexInfoDao.findbyBatchId();
	}


	@Override
	public String findIp(String ipaddress) {
		// TODO Auto-generated method stub
		return this.indexInfoDao.findIp(ipaddress);
	}
	
	
	
		
}
