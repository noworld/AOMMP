/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.conf;

/**
 * 
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 加载配置文件的通用接口描述接口
 */
public interface ConfigFile
{
	/**
	 * 用于判断配置文件是否已进行了修改
	 * @return
	 */
    public boolean isModified(String fileName);
    /**
     * 用于触发重新加载配置文件
     * @return
     */
    public boolean reLoad(String fileName);

}
