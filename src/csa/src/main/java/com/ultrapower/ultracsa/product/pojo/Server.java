/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;
import java.util.List;

/**
 * 作者: GuoDong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 镜像库或软件库服务地址信息
 */
public class Server {

	private Integer csaId;
	private Integer serverType;
	private String serverUrl;
	private String ip;
	private String serverStatusMark;
	private Date updateTime;
	private List<IP> ipList;
	
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getCsaId() {
		return csaId;
	}
	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}
	public Integer getServerType() {
		return serverType;
	}
	public void setServerType(Integer serverType) {
		this.serverType = serverType;
	}
	public String getServerUrl() {
		return serverUrl;
	}
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getServerStatusMark() {
		return serverStatusMark;
	}
	public void setServerStatusMark(String serverStatusMark) {
		this.serverStatusMark = serverStatusMark;
	}
	public List<IP> getIpList() {
		return ipList;
	}
	public void setIpList(List<IP> ipList) {
		this.ipList = ipList;
	}

}
