/*
 * PingUtil.java
 *
 * Created on 2003年2月28日, 上午9:27
 */

package com.ultrapower.ultracsa.product.util;


import java.io.*;
import java.util.*;

/**
 *
 * @author  Administrator
 */
public class PingUtil {

    /** Creates a new instance of MyPing */
    public PingUtil() {
    }


    /**
     *ping一个IP地址，看是否通
     *@retry -- 重试次数
     *@timeout -- 超时，单位：milleseconds
     *return true if alive, false if not alive
     */
    public static boolean ping(String s, int retry, int timeout)
    {
        //temporarily use execPing as ping utility
        boolean result = false;
        try{
            result = execPing(s,retry,timeout);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        
        return result;
    }
    
    /**
     *ping一个IP地址，看是否通
     *@retry -- 重试次数
     *@timeout -- 超时，单位：milleseconds
     *return true if alive, false if not alive
     */
    public static String pingResult(String s, int retry, int timeout)
    {
        //temporarily use execPing as ping utility
        String s1;
        String PING_CMD = "ping ";
        String osname = System.getProperty("os.name");
        if(osname.startsWith("SunOS") || osname.startsWith("Solaris"))
        {
            int sun_timeout = timeout/1000;
            if ( sun_timeout <=0 )
                sun_timeout = 1;
            PING_CMD = "/usr/sbin/ping ";
            s1 = new String(PING_CMD + " " + s + " " + sun_timeout);
        } else
        if(osname.startsWith("AIX"))
        {
            int aix_timeout = timeout/1000;
            if ( aix_timeout <=0 )
                aix_timeout = 1;
            PING_CMD = "/usr/sbin/ping ";
            s1 = new String(PING_CMD + " -c " + retry + " -w "+ aix_timeout + " " +s);
        } else
        if(osname.startsWith("HP-UX"))
        {
            int hp_timeout = timeout/1000;
            if ( hp_timeout <=0 )
                hp_timeout = 1;
            PING_CMD = "/usr/sbin/ping ";
            //s1 = new String(PING_CMD + " " + s + " -n "+ retry + " -m " + hp_timeout);
            s1 = new String(PING_CMD + " " + s + " -n "+ retry);
        }else
        if(osname.startsWith("Linux"))
        {
            PING_CMD = "/bin/ping -c 1 -w 1 ";
            s1 = new String(PING_CMD + " " + s);
        } else
        if(osname.startsWith("FreeBSD"))
        {
            PING_CMD = "/sbin/ping -c 1";
            s1 = new String(PING_CMD + " " + s);
        } else
        if(osname.startsWith("Windows"))
        {
            PING_CMD = "ping -n " + retry + " -w " + timeout;
            s1 = new String(PING_CMD + " " + s);
        } else
        {
            s1 = new String(PING_CMD + " " + s);
        }
        RunCmd runcmd = new RunCmd(s1);
        runcmd.start();
        do
            try
            {
                Thread.sleep(100L);
            }
            catch(Exception exception) { 
               // throw exception;
            }
        while(!runcmd.finished);
        
        if (runcmd.exceptionOccured != null && ! runcmd.exceptionOccured.trim().equalsIgnoreCase("")) {
            return runcmd.exceptionOccured.trim();
        }
            
      return runcmd.stdout.toString();
        
    }
    /**
     *增加一些额外的逻辑，0为不通，1为通，-1为发生了异常
     */
    public static int nPing(String s, int retry, int timeout){
        int result = -1;
        try{
            boolean temp = execPing(s,retry,timeout);
            if ( temp ){
                result = 1;
            }
            else{
                result = 0;
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            result = -1;
        }
        return result;
    }

    private static boolean execPing(String s, int retry ,int timeout) throws Exception
    {
        String s1;
        String PING_CMD = "ping ";
        String osname = System.getProperty("os.name");
        if(osname.startsWith("SunOS") || osname.startsWith("Solaris"))
        {
            int sun_timeout = timeout/1000;
            if ( sun_timeout <=0 )
                sun_timeout = 1;
            PING_CMD = "/usr/sbin/ping ";
            s1 = new String(PING_CMD + " " + s + " " + sun_timeout);
        } else
        if(osname.startsWith("AIX"))
        {
            int aix_timeout = timeout/1000;
            if ( aix_timeout <=0 )
                aix_timeout = 1;
            PING_CMD = "/usr/sbin/ping ";
            s1 = new String(PING_CMD + " -c " + retry + " -w "+ aix_timeout + " " +s);
        } else
        if(osname.startsWith("HP-UX"))
        {
            int hp_timeout = timeout/1000;
            if ( hp_timeout <=0 )
                hp_timeout = 1;
            PING_CMD = "/usr/sbin/ping ";
            //s1 = new String(PING_CMD + " " + s + " -n "+ retry + " -m " + hp_timeout);
            s1 = new String(PING_CMD + " " + s + " -n "+ retry);
        }else
        if(osname.startsWith("Linux"))
        {
            PING_CMD = "/bin/ping -c 1 -w 1 ";
            s1 = new String(PING_CMD + " " + s);
        } else
        if(osname.startsWith("FreeBSD"))
        {
            PING_CMD = "/sbin/ping -c 1";
            s1 = new String(PING_CMD + " " + s);
        } else
        if(osname.startsWith("Windows"))
        {
            PING_CMD = "ping -n " + retry + " -w " + timeout;
            s1 = new String(PING_CMD + " " + s);
        } else
        {
            s1 = new String(PING_CMD + " " + s);
        }
        RunCmd runcmd = new RunCmd(s1);
        runcmd.start();
        do
            try
            {
                Thread.sleep(100L);
            }
            catch(Exception exception) { 
                throw exception;
            }
        while(!runcmd.finished);
        
        if (runcmd.exceptionOccured != null && ! runcmd.exceptionOccured.trim().equalsIgnoreCase("")) {
            throw new Exception(runcmd.exceptionOccured);
        }
            
            
        if(osname.startsWith("Windows"))
        {
            if(!runcmd.result || runcmd.exitValue != 0)
                return false;
            String s2=runcmd.stdout.toString();
            return (s2.indexOf("Reply from") != -1 && s2.indexOf("bytes=") != -1) || (s2.indexOf("来自") != -1 && s2.indexOf("字节=") != -1);
        }
        if(osname.startsWith("Linux") || osname.startsWith("FreeBSD"))
        {
            if(!runcmd.result || runcmd.exitValue != 0)
                return false;
            String s3;
            return (s3 = runcmd.stdout.toString()).indexOf("64 bytes from") != -1;
        }
        return runcmd.result && runcmd.exitValue == 0;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if ( args.length != 3 )
        {
            System.out.println("Usage: java com.ultrapower.configuration.util.PingUtil [host] [retry] [timeout]");
            return;
        }
        System.out.println("ping:" + PingUtil.ping(args[0],Integer.parseInt(args[1]),Integer.parseInt(args[2])));
        System.out.println("nPing:" + PingUtil.nPing(args[0],Integer.parseInt(args[1]),Integer.parseInt(args[2])));
    }

}
