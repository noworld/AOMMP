/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.ultrapower.ultracsa.product.pojo.ShellFile;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 脚本文件模块service接口
 */
public interface ShellService {
	
	public void shellList(Model model);
	public List<String> getShellTab();
	public List<ShellFile> getShell(ShellFile file);
	public List<ShellFile> shellListByType(Model model,HttpServletRequest request,HttpServletResponse response);
	public long uploadShell(MultipartFile file,HttpServletRequest request,HttpServletResponse response);
	public boolean createShellFile(HttpServletRequest request,HttpServletResponse response);
	public boolean deleteShellFile(HttpServletRequest request,HttpServletResponse response);
}
