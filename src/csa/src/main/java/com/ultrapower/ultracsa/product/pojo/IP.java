package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;

/**
 * 
 * 作者: GuoPengFei
 * 
 * 日期: 2016 4 13
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO IP实体类
 */
public class IP {
	private Integer csaId;
	private String mac;
	private String ipAddress;
	private String mask;
	private String batChid;
	private String userAccount;
	private Date createTime;
	private String ipStatus;
	private Integer percent;
	private Integer mirrorLibraryId;
	private Server server;

	public IP(String mac, String ipAddress, String mask, String batChid,
			String userAccount) {
		this.mac = mac;
		this.ipAddress = ipAddress;
		this.mask = mask;
		this.batChid = batChid;
		this.userAccount = userAccount;

	}

	public IP() {

	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public Integer getMirrorLibraryId() {
		return mirrorLibraryId;
	}

	public void setMirrorLibraryId(Integer mirrorLibraryId) {
		this.mirrorLibraryId = mirrorLibraryId;
	}

	public Integer getCsaId() {
		return csaId;
	}

	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMask() {
		return mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

	public String getBatChid() {
		return batChid;
	}

	public void setBatChid(String batChid) {
		this.batChid = batChid;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getIpStatus() {
		return ipStatus;
	}

	public void setIpStatus(String ipStatus) {
		this.ipStatus = ipStatus;
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public Integer getPercent() {
		return percent;
	}

	public void setPercent(Integer percent) {
		this.percent = percent;
	}

}
