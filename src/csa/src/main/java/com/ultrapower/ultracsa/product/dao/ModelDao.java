/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.dao;

import java.util.List;

import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.RelSchemeIP;
import com.ultrapower.ultracsa.product.pojo.RelSchemeKS;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.pojo.ShellFile;
import com.ultrapower.ultracsa.product.pojo.Task;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于操作数据库中Task任务的Dao
 */
public interface ModelDao {
	
	/**
	 * 根据taskID 删除对应的Task
	 * @param taskid
	 */
	public void deleteTask(int taskid);
	/**
	 * 根据taskID 更新对应的Task的装填为已通知
	 * @param taskid
	 */
	public void updateTask(Task task);
	/**
	 * 根据Task的装填查询task
	 * @param statu  通知状态：0未通知 1已通知
	 * @return
	 */
	public List getTaskByStatus(int statu);
	
	/**
	 * 根据Task的tid查询task
	 * @param tid
	 * @return
	 */
	public Task getTaskByID(int tid);
	
	/**
	 * 根据ISO查询相关的Task
	 * @param isoID
	 * @return
	 */
	public List<Task> getTaskByISO(int isoID);
	
	/**
	 * 向系统中添加Task任务
	 * @param task
	 * @return
	 */
	public void insertTask(Task task);
	
	/**
	 * 向系统中添加ISO镜像库或软件库
	 * @param server
	 */
	public void insertServer(Server server);
	/**
	 * 更新ISO镜像库或软件库
	 * @param server
	 */
	public void updateServer(Server server);
	/**
	 * 根据CSAID查询镜像库或软件库
	 * @param csaid
	 * @return
	 */
	public Server getServerByID(int csaid);
	/**
	 * 通过IP查询Server
	 * @param ip
	 * @param type 表示Server的类型  0-镜像库  1-软件库
	 * @return
	 */
	public Server getServerByIP(Server server);
	/**
	 * 查询所有的镜像库或软件库
	 * @return
	 */
	public List<Server> getAllServer();
	/**
	 * 根据CSAID删除镜像库或软件库
	 * @param csaid
	 */
	public void deleteServer(int csaid);
	/**
	 * 查询当前数据库中脚本文件的类型
	 * @return
	 */
	public List<String> getShellType();
	/**
	 * 
	 * @param fileType
	 * @return
	 */
	public List<ShellFile> getShell(ShellFile file);
	/**
	 * 向数据库中增加脚本文件
	 * @param file
	 */
	public void insertShellFile(ShellFile file);
	
	/**
	 * 根据CSAID删除脚本清单
	 * @param csaid
	 */
	public boolean deleteShellFile(ShellFile file);
	
	/**
	 * 根据csaId查询对应的镜像
	 * @param csaID
	 * @return
	 */
	public Iso getISO(int csaID);
	

	/**
	 *  根据csaId删除对应的镜像
	 * @param csaID
	 */
	public void deleteISO(int csaID);
	
	/**
	 * 查询所有的脚本
	 * @return
	 */
	public List<ShellFile> ShellFileList();
	
	/**
	 * 根据镜像名称查询镜像
	 * @param isoName
	 * @return
	 */
	public Iso getISOByName(String isoName);
	/**
	 * 根据file名称获取shell 对象
	 * @param fileName
	 * @return
	 */
	public ShellFile getShellFileByName(String fileName);
	
	/**
	 * 查询安装方案与部署配置的关系
	 * @return
	 */
	public List<RelSchemeKS> getRelSchemeKS(RelSchemeKS sks);
	
	/**
	 * 向数据库中插入安装方案与部署方案的关系
	 * @param sks
	 */
	public void  insertRelSchemeKS(RelSchemeKS sks);
	
	/**
	 * 从数据库中删除安装方案与部署方案的关系
	 * @param sks
	 */
	public void deleteRelSchemeKS(RelSchemeKS sks);
	
	/**
	 * 查询安装方案与IP的关系
	 * @return
	 */
	public List<RelSchemeIP> getRelSchemeIP(RelSchemeIP sip);
	
	/**
	 * 向数据库中插入安装方案与IP的关系
	 * @param sks
	 */
	public void  insertRelSchemeIP(RelSchemeIP sip);
	
	/**
	 * 从数据库中删除安装方案与IP的关系
	 * @param sks
	 */
	public void deleteRelSchemeIP(RelSchemeIP sip);
	
	/**
	 * 获取系统中的有效Task列表，有效是指：Task关联的镜像库存在
	 * @return
	 */
	public List<Task> getValidityTask();
	
	
}
