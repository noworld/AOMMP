/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 声明用于加载各种系统基础服务的任务调度接口
 */
public interface ModuleServer {
	/**
	 * 启动server
	 * @return
	 */
	public boolean startServer() throws Exception;
	/**
	 * 停止server
	 * @return
	 */
	public boolean stopServer()throws Exception;
	/**
	 * 重新启动Server
	 * @return
	 */
	public boolean resetServer()throws Exception;

	/**
	 * x想server中注入对象
	 * @param obj
	 */
	public void addObject(Object obj);
}
