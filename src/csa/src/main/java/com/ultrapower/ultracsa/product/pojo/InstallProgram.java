/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;

/**
 * 作者: guodong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 安装方案实体类
 */
public class InstallProgram {

	private Integer csaId;
	private Integer ksCsaId;
	private String schemeName;
	private String userAccount;
	private Date createTime;
	private Integer schemeStatus;
	private Integer installStatus;
	private String applications;
	/*
	 * 当前方案所需安装OS的总数量
	 */
	private Integer installSum;
	/*
	 * 当前方案已完成安装OS的数量
	 */
	private Integer installFinished;
	/*
	 * 当前方案正在执行安装OS的数量
	 */
	private Integer installing;
	/*
	 * 当前方案安装OS失败的数量
	 */
	private Integer installError;
	private KickStart kickStart;
	public Integer getCsaId() {
		return csaId;
	}
	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}
	public Integer getKsCsaId() {
		return ksCsaId;
	}
	public void setKsCsaId(Integer ksCsaId) {
		this.ksCsaId = ksCsaId;
	}
	public String getSchemeName() {
		return schemeName;
	}
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public Integer getSchemeStatus() {
		return schemeStatus;
	}
	public void setSchemeStatus(Integer schemeStatus) {
		this.schemeStatus = schemeStatus;
	}
	public Integer getInstallStatus() {
		return installStatus;
	}
	public void setInstallStatus(Integer installStatus) {
		this.installStatus = installStatus;
	}
	public String getApplications() {
		return applications;
	}
	public void setApplications(String applications) {
		this.applications = applications;
	}
	public KickStart getKickStart() {
		return kickStart;
	}
	public void setKickStart(KickStart kickStart) {
		this.kickStart = kickStart;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getInstallSum() {
		return installSum;
	}
	public void setInstallSum(Integer installSum) {
		this.installSum = installSum;
	}
	public Integer getInstallFinished() {
		return installFinished;
	}
	public void setInstallFinished(Integer installFinished) {
		this.installFinished = installFinished;
	}
	public Integer getInstalling() {
		return installing;
	}
	public void setInstalling(Integer installing) {
		this.installing = installing;
	}
	public Integer getInstallError() {
		return installError;
	}
	public void setInstallError(Integer installError) {
		this.installError = installError;
	}
	
	
}
