/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module.taskqueue;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.dao.InstallProgramDao;
import com.ultrapower.ultracsa.product.dao.ModelDao;
import com.ultrapower.ultracsa.product.dao.UploadDao;
import com.ultrapower.ultracsa.product.module.AbstractModuleServer;
import com.ultrapower.ultracsa.product.pojo.InstallProgram;
import com.ultrapower.ultracsa.product.pojo.Task;
import com.ultrapower.ultracsa.product.util.BeanUtil;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 第七对task进行状态汇总，更新安装方案的状态信息
 */
public class SchemeStatusReport extends AbstractModuleServer {
	private static Logger log=Logger.getLogger(SchemeStatusReport.class);

	/* (non-Javadoc)
	 * @see com.ultrapower.ultracsa.product.module.AbstractModuleServer#startServer()
	 */
	@Override
	public boolean startServer() throws Exception {
		Thread thr=new Thread("Task状态监听"){
			public void run(){
				while(true){
					List<Task> list=BeanUtil.getBean(ModelDao.class).getValidityTask();
					if(list!=null){
						Map<Integer,int[]> map=new HashMap();
						for(Task task: list){
							Integer schid=task.getSchemeCsaId();
							//0--安装完成  1--安装中  2--安装失败 
							int[] num=map.get(schid);
							if(num==null){
								num=new int[3];
								map.put(schid,num);
							}
							
							if(task.getTaskStatus()==0)
								num[1]++;
							else{
								num[0]++;
								if(task.getTaskStatus()==2)
									num[2]++;
							}							
						}
						
						InstallProgramDao dao=BeanUtil.getBean(InstallProgramDao.class);
						InstallProgram sch=null;
						for (Iterator iterator = map.keySet().iterator(); iterator
								.hasNext();) {
							Integer schid = (Integer) iterator.next();
							sch=new InstallProgram();
							sch.setCsaId(schid);
							int[] num=map.get(schid);
							sch.setInstallFinished(num[0]);
							sch.setInstalling(num[1]);
							sch.setInstallError(num[2]);
							sch.setInstallSum(num[0]+num[1]);
							dao.updateInstallInf(sch);
						}
					}
					try {
						sleep(15*1000);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
			}
		};
		thr.start();
		return true;
	}

}
