package com.ultrapower.ultracsa.product.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ultrapower.ultracsa.product.service.IndexInfoService;
import com.ultrapower.ultracsa.product.service.OSDeployService;
import com.ultrapower.ultracsa.product.service.ShellService;

@Controller
public class IndexInfoController {
	@Resource
	private IndexInfoService indextInfoService;
	@Resource  
    private ShellService service; 
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/home" )
	public String shellList(Model model){
		service.shellList(model);
		return "index";
	}
	@RequestMapping(value = "index_osbs")
	public String getIPConfig(Map<String, Object> model) {

		return "index_osbs";
	}
	
	
	@RequestMapping(value = "/findbyIpStatus", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> findbyIpStatus(){
		List<Map<String,String>>  map1 = new ArrayList<Map<String,String>>(); 
		List<Map<String,String>>  map2 = new ArrayList<Map<String,String>>(); 
		List<Map<String,String>>  map3= new ArrayList<Map<String,String>>(); 
		map1=indextInfoService.findbyIpStatus();
		map2=indextInfoService.findbyIsoType();
		map3=indextInfoService.findbySchemeSyatus();
		Map<String,Object> mapa =new HashMap<String,Object>();
		mapa.put("IpStatus1", map1);
		mapa.put("IsoType1", map2);
		mapa.put("SchemeSyatus", map3);
		
		return mapa;
	}
	
	@RequestMapping(value = "/findbyBatchId", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String,String>> findbyBatchId(){   
		// Map<String,List<String>> map =  new  HashMap<String,List<String>>(); 
		System.out.println("asdsad");
		List<Map<String,String>>  list= new ArrayList<Map<String,String>>(); 
		list = indextInfoService.findbyBatchId();
		 return list;
		   
		}
	@RequestMapping(value="/findIp",  method = RequestMethod.POST)
	@ResponseBody
	/**
	 * 
	 * 判断IP是否存在
	 */
	public boolean findIp(String ipaddress){
		boolean b = false;
		String i= indextInfoService.findIp(ipaddress);
		
	if(null==i){
			b=false;
	}else if(i.equals(ipaddress)){
			b=true;
		}
		return b;
	}		
	

		
	}
