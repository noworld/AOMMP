/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.ultrapower.ultracsa.product.pojo.ShellFile;
import com.ultrapower.ultracsa.product.service.ShellService;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 控制脚本清单模块相关请求
 */
@Controller
public class ShellController<T> {
	private static Logger logger = Logger.getLogger(ShellController.class);  
	
	@Resource  
    private ShellService service; 

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/shellview" )
	public String shellList(Model model){
		service.shellList(model);
		return "shell/shellmanage";
	}
	
	/**
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/getTabList" )
	public ResponseEntity<List<String>> getShellTab(Model model, HttpServletRequest request,HttpServletResponse response){
		return 	new ResponseEntity<List<String>>(service.getShellTab(), HttpStatus.OK);
	}
	
	/**
	 * 按脚本模块类型显示对应的脚本
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shellListbyType" ,method=RequestMethod.POST ,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ShellFile>> shellListByType(Model model, HttpServletRequest request,HttpServletResponse response){
		return 	new ResponseEntity<List<ShellFile>>(service.shellListByType(model,request,response), HttpStatus.OK);
	}
	
	/**
	 * 检查脚本文件是否已经上传
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/checkFileName",method = RequestMethod.POST)
	public ResponseEntity<Boolean>  checkFileName(Model model,HttpServletRequest request,HttpServletResponse response){
		String fileName=request.getParameter("fileName");
		ShellFile shellFile=new ShellFile();
		shellFile.setFileName(fileName);
		List<ShellFile> list=service.getShell(shellFile);
		if(list!=null && list.size()>0){
			for (ShellFile shellFile2 : list) {
				if(shellFile2.getFileName().equals(fileName))
					return new ResponseEntity<Boolean>(new Boolean(true), HttpStatus.OK); 
			}
		}
		return new ResponseEntity<Boolean>(new Boolean(false), HttpStatus.OK); 
	}
	
	/**
	 * 检查脚本文件是否已经上传
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/findFile",method = RequestMethod.POST)
	public ResponseEntity<List<ShellFile>>  findFile(Model model,HttpServletRequest request,HttpServletResponse response){
		String fileName=request.getParameter("fileName");
		ShellFile shellFile=new ShellFile();
		shellFile.setFileName(fileName);
		List<ShellFile> list=service.getShell(shellFile);
		if(list!=null && list.size()>0){
			return new ResponseEntity<List<ShellFile>>(list, HttpStatus.OK); 
		}
		return new ResponseEntity<List<ShellFile>>(new ArrayList(), HttpStatus.OK); 
	}
	
	/**
	 * 上传脚本文件到Web服务器
	 * @param file
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/uploadShell",method = RequestMethod.POST)
	public ResponseEntity<Long>  uploadShell(@RequestParam("file")MultipartFile file,HttpServletRequest request,HttpServletResponse response){
		return new ResponseEntity<Long>(service.uploadShell(file,request, response), HttpStatus.OK);
	}
	
	/**
	 * 向数据库中增加脚本文件记录
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/createShellFile" ,method=RequestMethod.POST)
	public ResponseEntity<Boolean> createShellFile(HttpServletRequest request,HttpServletResponse response){
		return new ResponseEntity<Boolean>(service.createShellFile(request, response), HttpStatus.OK);
	}
	
	/**
	 * 向数据库中增加脚本文件记录
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/deleteShellFile" ,method=RequestMethod.POST)
	public ResponseEntity<Boolean> deleteShellFile(HttpServletRequest request,HttpServletResponse response){
		return new ResponseEntity<Boolean>(service.deleteShellFile(request, response), HttpStatus.OK);
	}
	
}
