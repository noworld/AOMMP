package com.ultrapower.ultracsa.product.dao;

import java.util.List;
import java.util.Map;
/**
 * 作者: jinzheshen
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 安装方案查询dao
 */
//首页查询数量等数据
public interface IndexInfoDao {
	public List<Map<String,String>> findbyIpStatus();
	public List<Map<String,String>>  findbyIsoType();
	public List<Map<String,String>> findbySchemeSyatus();
	public List<Map<String,String>> findbyBatchId();
	public String findIp(String ipaddress);
	}
