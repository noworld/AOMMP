package com.ultrapower.ultracsa.product.controller;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import com.ultrapower.ultracsa.product.pojo.User;
import com.ultrapower.ultracsa.product.pojo.UserListForm;
import com.ultrapower.ultracsa.product.service.UserService;

@Controller
@SessionAttributes("userName")
public class UserController {
	
	@Resource  
    private UserService userService;  
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
    public String login(User userInfo,Model model,SessionStatus sessionStatus,HttpSession httpSession){
		model.addAttribute("userName", userInfo.getUserName());
    	return String.valueOf(userService.login(userInfo,sessionStatus,httpSession));
    }
	
	@RequestMapping(value="/loginError")
    public String loginError(Model model){
		model.addAttribute("errorMsg", "errorMsg");
    	return "login";
    }
	
	@RequestMapping(value="/loginSuccess")
    public String loginSuccess(Model model){
    	return "index";
    }
	
	@RequestMapping("/showUser")  
    public String toIndex(User newUser,Model model){  
        //int userId = Integer.parseInt(request.getParameter("id"));  
        User user = this.userService.getUserById(new Integer(1));
        List<User> userList = userService.getListByAge(13);
        //this.userService.insertUser(newUser);
        //System.out.println("新增id为"+newUser.getId());
        model.addAttribute("userList", userList);
        model.addAttribute("user", user);  
        return "showUser";  
    } 
	
	//@RequestMapping("/testListForm")
	//List需要绑定在对象上，而不能直接写在Controller方法的参数中
	/*public void testListForm(UserListForm userForm) {
	    for (User user : userForm.getUsers()) {
	        System.out.println(user.getUserName() + " - " + user.getPassword());
	    }
	}*/
	
	@RequestMapping(value="/helloWorld",method=RequestMethod.GET)
	//加上@RequestParam参数必传，否则页面会报404
	//返回参数可以是Map也可以是Model
	//@RequestParam(value="XXX",required=false)
	/*public String helloWorld(@RequestParam("username")  String username, Map<String,Object> model){
		model.put("username",username);
		return "hello";
	}*/
	//接受参数时，表单name值必须和类的属性值相同
	//返回页面数据Model
	public String helloWorld(User username, Model model){
		//此时返回参数的key为返回对象的类型(首字母小写)相当于("string",username)
		//如果参数username是User的实例,("user",username)
		//model.addAttribute(username);
		model.addAttribute("model", "testmodel111");
		// return "success"; //跳转到success页面
		return "showUser";
	}
	
	//@RequestMapping(value="/user/new/{id:\\d+}/{userName}",method=RequestMethod.GET)
	//id可通过 @Pathvariable注解绑定它传过来的值到方法的参数上
	//若方法参数名称和需要绑定的uri template中变量名称不一致，需要在@PathVariable("***")指定uri template中的名称
	/*public String testRestful(@PathVariable("id")Integer id,@PathVariable("userName")String group){
		System.out.println("id:"+id+"userName:"+userName);
		return "showUser";
	}*/
}
