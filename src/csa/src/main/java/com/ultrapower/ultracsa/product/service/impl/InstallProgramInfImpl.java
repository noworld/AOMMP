/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ultrapower.ultracsa.product.dao.InstallProgramDao;
import com.ultrapower.ultracsa.product.dao.KickstartDao;
import com.ultrapower.ultracsa.product.dao.UploadDao;
import com.ultrapower.ultracsa.product.module.TaskOperator;
import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.pojo.InstallProgram;
import com.ultrapower.ultracsa.product.pojo.KickStart;
import com.ultrapower.ultracsa.product.pojo.Task;
import com.ultrapower.ultracsa.product.service.InstallProgramService;

/**
 * 作者: guodong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 安装方案服务层实现类
 */
@Service("installProgramService")
@Transactional
public class InstallProgramInfImpl implements InstallProgramService{

	@Resource
    private InstallProgramDao installProgramDao;
	@Resource
	private KickstartDao kickstartDao;

	@Override
	public List<InstallProgram> getInstallPlanByStatu(Integer status) {
		List<InstallProgram> installProgramList = installProgramDao.getInstallPlanByStatu(status);
		for(InstallProgram i:installProgramList){
			List<IP> ipList = installProgramDao.getIpByScheme(i.getCsaId());
			KickStart kickStart = kickstartDao.findKickStartById(i.getKsCsaId());
			StringBuffer ip = new StringBuffer();
			for(IP p:ipList){
				ip.append(p.getIpAddress()).append(";");
			}
			i.setApplications(ip+"");
		}
		return installProgramList;
	}
	public List<IP> getIpByScheme(Integer id){
		List<IP> ipList = installProgramDao.getIpByScheme(id);
		return ipList;
	}
	@Override
	public List<IP> getIpGroup(Integer csaid) {
		List<IP> ipList = installProgramDao.getIpGroup(csaid);
		return ipList;
	}
	@Override
	public InstallProgram getInstallPlanById(Integer id) {
		InstallProgram installProgram = installProgramDao.getInstallPlanById(id);
		return installProgram;
	}

	@Override
	public Integer insertScheme(InstallProgram installProgram,String[] ipGroup,HttpSession httpSession) {
		installProgram.setCsaId(null);
		Map<String, Object> map = new HashMap<String, Object>();
		installProgram.setUserAccount(httpSession.getAttribute("userName")+"");
		installProgramDao.insertScheme(installProgram);
		for(String i:ipGroup){
			map.put("schcsaid", installProgram.getCsaId());
			map.put("ipcsaid", i);
			installProgramDao.insertRelSchemeIp(map);
			
		}
		return installProgram.getCsaId();
	}
	
	public void updateScheme(InstallProgram installProgram,String[] ipGroup){
		Map<String, Object> map = new HashMap<String, Object>();
		installProgramDao.updateScheme(installProgram);
		installProgramDao.deleteRelSchemeIp(installProgram.getCsaId());
		for(String i:ipGroup){
			map.put("schcsaid", installProgram.getCsaId());
			map.put("ipcsaid", i);
			installProgramDao.insertRelSchemeIp(map);
		}
	}
	@Override
	public void updateSchemeStatus(Integer[] idArr, Integer status) {
		List<Map> list = new ArrayList<Map>();
		for(Integer i:idArr){
			Map<String, Object> param=new HashMap<String, Object>();
			param.put("id", i);
			param.put("status", status);
			if(status==4){
				param.put("install", 1);
			}
			list.add(param);
		}
		if(status==0){
			installProgramDao.deleteScheme(idArr);
			installProgramDao.deleteRelSchemeIpBatch(idArr);
		}else{
			installProgramDao.updateSchemeStatus(list);
			if(status==4){
				for(Integer id:idArr){
					Task task = new Task();
					task.setSchemeCsaId(id);
					installProgramDao.insertInstallIsoTask(task);
					task.setCreateTime(new Date());
					task.setTaskStatus(0);
					task.setTaskType(3);
					task.setNoticeStatus(0);
					TaskOperator.getInstance().addTask(task, null);
				}
			}
		}
	}

}
