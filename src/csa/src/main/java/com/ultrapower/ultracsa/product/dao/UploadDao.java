/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.pojo.Task;

/**
 * 作者: GuoDong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 镜像上传DAO
 */
public interface UploadDao {
	public List<Server> findServer(@Param("type")Integer type);
	public void insertIso(Iso iso);
	public void updateIso(Iso iso);
	public List<Task> findTaskById(Integer id);
	public List<Iso> findIso();
	public List<Iso> findIsoType();
	public void deleteIso(List list);
	public void insertIsoTask(Task task);
	public void insertDeleteIsoTask(Task task);
	public Iso findIsoById(Integer id);
	public void insertFile(Iso iso);
}
