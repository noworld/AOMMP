package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;
/**
 * 
 * 作者: GuoPengFei
 * 
 * 日期: 2016 5 12
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public class KickstartLog {
	private Integer csaId; // 主键
	private String userAccount; // 用户名
	private Date updateTime; // 修改时间
	private String userIp; // 进行此操作的客户机IP
	private String notes; // 修改说明
	private Integer kickstartCsaid; // 修改的IP

	public KickstartLog(String userAccount, Date updateTime, String userIp,
			String notes, Integer kickstartCsaid) {
		this.userAccount = userAccount;
		this.updateTime = updateTime;
		this.userIp = userIp;
		this.notes = notes;
		this.kickstartCsaid = kickstartCsaid;
	}

	public Integer getCsaId() {
		return csaId;
	}

	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUserIp() {
		return userIp;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getKickstartCsaid() {
		return kickstartCsaid;
	}

	public void setKickstartCsaid(Integer kickstartCsaid) {
		this.kickstartCsaid = kickstartCsaid;
	}

}
