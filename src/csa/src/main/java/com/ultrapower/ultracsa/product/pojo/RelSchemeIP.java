/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于描述安装方案与IP的关系
 */
public class RelSchemeIP {
	/**/
	private Integer csaId;
	/*安装方案ID*/
	private Integer schCsaId;
	/*部署配置ID*/
	private Integer ipCsaId;
	public Integer getCsaId() {
		return csaId;
	}
	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}
	public Integer getSchCsaId() {
		return schCsaId;
	}
	public void setSchCsaId(Integer schCsaId) {
		this.schCsaId = schCsaId;
	}
	public Integer getIpCsaId() {
		return ipCsaId;
	}
	public void setIpCsaId(Integer ipCsaId) {
		this.ipCsaId = ipCsaId;
	}
	
	
}
