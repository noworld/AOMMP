/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.pojo.InstallProgram;
import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.KickStart;
import com.ultrapower.ultracsa.product.service.InstallProgramService;
import com.ultrapower.ultracsa.product.service.impl.OSDeployImpl;


/**
 * 作者: guodong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 装机方案
 */
@Controller
public class InstallPlanController {
	private static Logger logger = Logger.getLogger(InstallPlanController.class);
	
	@Resource
	private InstallProgramService installProgramService;
	@Resource
	private OSDeployImpl oSDeployImpl;
	
	@RequestMapping(value = "/installProgram")
	public String installProgram(Model model){
		return"deploy/installation_program";
	}
	
	@RequestMapping(value = "/getInstallPlan", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getInstallPlan(Integer status,Model model){
		Map<String, Object> modelMap = new HashMap<String, Object>();
		List<InstallProgram> installProgramList = installProgramService.getInstallPlanByStatu(status);
		modelMap.put("installProgramList", installProgramList);
		return modelMap;
	}
	@RequestMapping(value = "/saveInstallProgramInfo",method = RequestMethod.POST)
	@ResponseBody
    public Integer saveInstallProgramInfo(InstallProgram installProgram,String[] ipGroup,HttpSession httpSession) {
		Integer newId = installProgramService.insertScheme(installProgram,ipGroup,httpSession);
        return newId;
    }
	@RequestMapping(value = "/updateInstallProgramInfo",method = RequestMethod.POST)
	@ResponseBody
    public String updateInstallProgramInfo(InstallProgram installProgram,String[] ipGroup) {
		System.out.println(installProgram.getCsaId());
		installProgramService.updateScheme(installProgram,ipGroup);
        return "true";
    }
	@RequestMapping(value = "/getAllKs",method = RequestMethod.POST)
	@ResponseBody
    public Map<String, Object> getAllKs(Integer csaId,Model model) {
		Map<String, Object> modelMap = new HashMap<String, Object>();
		List<KickStart> kickStartList = oSDeployImpl.kickStartList();
		List<IP> ipList = installProgramService.getIpGroup(csaId);
		if(csaId!=null){
			List<IP> chosenipList = installProgramService.getIpByScheme(csaId);
			InstallProgram installProgram = installProgramService.getInstallPlanById(csaId);
			modelMap.put("installProgram", installProgram);
			modelMap.put("chosenipList", chosenipList);
		}
		modelMap.put("kickStartList", kickStartList);
		modelMap.put("ipList", ipList);
        return modelMap;
    }
	@RequestMapping(value = "/updateSchemeStatus",method = RequestMethod.POST)
	@ResponseBody
    public String updateSchemeStatus(@RequestParam(value="idArr[]", required=false)Integer[] idArr,Integer status) {
		installProgramService.updateSchemeStatus(idArr, status);
        return "true";
    }
}
