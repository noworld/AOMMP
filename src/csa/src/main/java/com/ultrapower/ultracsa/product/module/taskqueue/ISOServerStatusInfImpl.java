/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module.taskqueue;

import java.util.List;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.dao.ModelDao;
import com.ultrapower.ultracsa.product.module.AbstractModuleServer;
import com.ultrapower.ultracsa.product.module.TaskOperator;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.util.BeanUtil;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 对ISO进行库状态进行监听，负责维护异常Task队列中的数据
 */
public class ISOServerStatusInfImpl extends AbstractModuleServer {
	private static Logger log=Logger.getLogger(ISOServerStatusInfImpl.class);
	/* (non-Javadoc)
	 * @see com.ultrapower.ultracsa.product.module.ModuleServer#startServer()
	 */
	@Override
	public boolean startServer() throws Exception {
		Thread thr=new Thread("镜像库状态监测 "){
			public void run(){
				while(true){
				//检查ISO状态
					ModelDao dao=BeanUtil.getBean(ModelDao.class);
					List<Server> list=dao.getAllServer();
					long time=System.currentTimeMillis();
					if(list!=null){
						for (Server server : list) {				
							if("0".equals(server.getServerStatusMark())){
								//需要镜像库没5秒钟反馈一次状态
								if(time-server.getUpdateTime().getTime()>5000){
									server.setServerStatusMark("1");
									dao.updateServer(server);
								}else{
									//如果状态恢复了，则：
									TaskOperator.getInstance().recoverTask(server.getCsaId());
								}
							}else{
								//如果ISO镜像被删除，则删除Task,下面一行应该在删除当做发生时执行:
								//TaskOperator.getInstance().removeTask(server.getCsaId());
							}
						}
					}
					try {
						sleep(5*1000);
					} catch (InterruptedException e) {
						log.error("",e);
					}
				}
			}
		};
		thr.start();
		return true;
	}
}
