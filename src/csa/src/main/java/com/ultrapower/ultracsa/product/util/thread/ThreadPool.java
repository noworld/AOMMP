/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.util.thread;

import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.conf.ProjectConfigServer;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 对产品的线程进行管理，提供按模块进行线程池的申请
 */
public class ThreadPool {
	private static Logger log=Logger.getLogger(ThreadPool.class);
	
	private static Hashtable<String,ExecutorService> threadpoolMap=new Hashtable();
	
	/**
	 * 按模块申请线程池
	 * @param module 模块名称
	 * @return
	 */
	public static void registerTreadPool(String module){
		if(!threadpoolMap.containsKey(module)){
			int intSize=5;
			String size=ProjectConfigServer.getConfig("core","thread_pool_size");
			if(size!=null)
				intSize=Integer.parseInt(size);
			ExecutorService service=Executors.newFixedThreadPool(intSize);
			threadpoolMap.put(module, service);
			System.out.println(threadpoolMap);
		}
	}
		
		/**
		 * 按模块申请线程池
		 * @param module 模块名称
		 * @param poolSize 线程池数量
		 * @return
		 */
		public static void registerTreadPool(String module,int poolSize){
			ExecutorService service=null;
			if(threadpoolMap.containsKey(module))
				service=threadpoolMap.get(module);
			else{
				if(poolSize<=0){
					poolSize=5;
				}
				service=Executors.newFixedThreadPool(poolSize);
			}
		
	}
	
	public static void execTask(String moduleName,Runnable job) throws Exception{
		if(!threadpoolMap.containsKey(moduleName))
			throw new Exception("模块："+moduleName+" 没有注册线程池！");
		threadpoolMap.get(moduleName).execute(job);
	}
}
