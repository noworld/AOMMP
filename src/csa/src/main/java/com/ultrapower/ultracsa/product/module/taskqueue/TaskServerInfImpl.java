/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module.taskqueue;

import java.util.List;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.dao.ModelDao;
import com.ultrapower.ultracsa.product.module.AbstractModuleServer;
import com.ultrapower.ultracsa.product.module.ISOTaskHook;
import com.ultrapower.ultracsa.product.module.TaskOperator;
import com.ultrapower.ultracsa.product.pojo.Task;
import com.ultrapower.ultracsa.product.util.BeanUtil;
import com.ultrapower.ultracsa.product.util.thread.ThreadPool;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于管理需要通知镜像库或软件库的待通知Task，已经通知或完成（包含失败）的Task不在此服务管理范围
 */
public class TaskServerInfImpl extends AbstractModuleServer{
	private static Logger log=Logger.getLogger(TaskServerInfImpl.class);
	
	/**
	 * 
	 */
	public TaskServerInfImpl() {
		ThreadPool.registerTreadPool("ISO_TASK");
		ThreadPool.registerTreadPool("INSTALL_TASK");
	}

	/* (non-Javadoc)
	 * @see com.ultrapower.ultracsa.product.module.AbstractModuleServer#startServer()
	 */
	@Override
	public boolean startServer() throws Exception {		
		//从数据库中加载未通知的Task
		List<Task> list=BeanUtil.getBean(ModelDao.class).getTaskByStatus(0);
		if(list!=null){
			//将任务添加到状态轮训队列
			for (Task task : list) {
				TaskOperator.getInstance().addTask(task,new ISOTaskHook());
			}
		}
		
		return true;
	}  
	
	public void addObject(Object obj){
		if(obj instanceof Task)
			TaskOperator.getInstance().addTask((Task)obj,new ISOTaskHook());
	}
}
