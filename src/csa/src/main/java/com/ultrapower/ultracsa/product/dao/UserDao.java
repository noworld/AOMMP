package com.ultrapower.ultracsa.product.dao;

import java.util.List;

import com.ultrapower.ultracsa.product.pojo.Article;
import com.ultrapower.ultracsa.product.pojo.User;

public interface UserDao {
	public User findById(Integer id);
	public List getListByAge(Integer age);
	public void insertUser(User user);
	public Article getUserArticles(Integer id);
	public User findUser(User user);
}
