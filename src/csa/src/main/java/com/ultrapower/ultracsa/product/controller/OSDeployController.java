package com.ultrapower.ultracsa.product.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.ultrapower.ultracsa.product.service.OSDeployService;

@Controller
public class OSDeployController {

	private static final int DEFAULTROW = 20;
	@Resource
	private OSDeployService osDeployService;

	@RequestMapping(value = "/osdeploy")
	public String osDeplor(Map<String, Object> model) {
		List<String> osList = osDeployService.findIsoList();
		Map<String, Object> packages = osDeployService.packagesList();

		Map<String, Object> shellFileMap = osDeployService.findAllShell();

		System.out.println(osList);
		model.put("osList", osList);
		// model.put("KsList", ksList);
		model.put("packages", packages);
		model.put("shellFileMap", shellFileMap);
		return "deploy/os_deploy_config";
	}

	@RequestMapping(value = "getKsData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getKsData(@RequestBody Map<String, String> map,
			HttpServletRequest request) {
		RequestContext requestContext = new RequestContext(request);
		int currentPage = Integer.parseInt(map.get("currentPage"));
		String ks_search = map.get("ks_search");
		Map<String, Object> map1 = osDeployService.KsList(ks_search,
				currentPage, DEFAULTROW);
		return map1;
	}

	/**
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "editKickstart", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> editKickstart(
			@RequestBody Map<String, Object> map, HttpSession session,HttpServletRequest request) {
		RequestContext requestContext  = new RequestContext(request);
		String userIp = getRequestIp(request);
		String user = (String) session.getAttribute("userName");
		map.put("userIp", userIp);
		Map<String, Object> map1 = osDeployService.editKickstart(map, user,requestContext);
		return map1;
	}
	/**
	 * 获取请求客户端IP,解决客户端多层代理获取不到真正IP的问题
	 * @param request
	 * @return
	 */
	private String getRequestIp(HttpServletRequest request){

		 String ip = request.getHeader("x-forwarded-for");
		 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		  ip = request.getHeader("Proxy-Client-IP");
		 }
		 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		  ip = request.getHeader("WL-Proxy-Client-IP");
		 }
		 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		  ip = request.getRemoteAddr();
		 }
		 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		  ip = request.getHeader("http_client_ip");
		 }
		 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		  ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		 }
		 // 如果是多级代理，那么取第一个ip为客户ip
		 if (ip != null && ip.indexOf(",") != -1) {
		  ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
		 }
		 return ip;
	}
	@RequestMapping(value = "deleteKickstarts", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteKickstarts(
			@RequestBody Map<String, String> map, HttpSession session) {
		Map<String, Object> resultMap = osDeployService.delteKickStarts(map);
		return resultMap;
	}

	@RequestMapping(value = "getKsFile", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getKsFile(@RequestBody Map<String, String> map,
			HttpServletRequest request) {
		RequestContext requestContext = new RequestContext(request);
		Map<String, Object> resultMap = osDeployService.getKsFile(map,
				requestContext);
		return resultMap;
	}

	@RequestMapping(value = "copyKickstart", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> copyKickstart(
			@RequestBody Map<String, String> map, HttpSession session,HttpServletRequest request) {
		RequestContext requestContext = new  RequestContext(request);
		String user = (String) session.getAttribute("userName");
		// String user ="root";
		map.put("user", user);
		Map<String, Object> resultMap = osDeployService.copyKickstart(map,requestContext);
		return resultMap;
	}

}
