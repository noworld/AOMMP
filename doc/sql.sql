/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.11-log : Database - csadb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`csadb` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `csa_user` */

DROP TABLE IF EXISTS `csa_user`;

CREATE TABLE `csa_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `csa_user` */

insert  into `csa_user`(`ID`,`USERNAME`,`PASSWORD`) values (1,'root','63a9f0ea7bb98050796b649e85481845');

/*Table structure for table `inventory_host` */

DROP TABLE IF EXISTS `inventory_host`;

CREATE TABLE `inventory_host` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUPNAME` varchar(20) NOT NULL,
  `IPADDRESS` varchar(28) NOT NULL,
  `LOGINUSER` varchar(30) NOT NULL,
  `USEAIM` varchar(2000) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inventory_host` */

/*Table structure for table `inventory_ip` */

DROP TABLE IF EXISTS `inventory_ip`;

CREATE TABLE `inventory_ip` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `MAC` varchar(17) NOT NULL,
  `IPADDRESS` varchar(28) NOT NULL,
  `MASK` varchar(15) NOT NULL,
  `BATCHID` varchar(100) NOT NULL,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `IPSTATUS` varchar(1000) NOT NULL,
  `PERCENT` int(11) DEFAULT NULL,
  `MIRRORLIBRARYID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=321 DEFAULT CHARSET=utf8;

/*Data for the table `inventory_ip` */

insert  into `inventory_ip`(`CSAID`,`MAC`,`IPADDRESS`,`MASK`,`BATCHID`,`USERACCOUNT`,`CREATETIME`,`IPSTATUS`,`PERCENT`,`MIRRORLIBRARYID`) values (260,'A3:39:DD:DD:DD:DD','192.168.12.7','255.255.255.0','q20160423','root','2016-05-16 11:07:08','2',NULL,NULL),(261,'A3:40:DD:DD:DD:DD','192.168.12.8','255.255.255.0','q20160424','root','2016-05-16 11:07:08','2',NULL,NULL),(262,'A3:41:DD:DD:DD:DD','192.168.12.9','255.255.255.0','q20160425','root','2016-05-16 11:07:08','3',NULL,NULL),(267,'A3:46:DD:DD:DD:DD','192.168.12.14','255.255.255.0','q20160430','root','2016-05-16 11:07:08','1',NULL,NULL),(268,'A3:47:DD:DD:DD:DD','192.168.12.15','255.255.255.0','q20160431','root','2016-05-16 11:07:08','1',NULL,NULL),(290,'A3:33:DD:DD:DD:DD','192.168.12.1','255.255.255.0','q20160417','root','2016-05-16 13:06:39','1',NULL,NULL),(291,'A3:34:DD:DD:DD:DD','192.168.12.2','255.255.255.0','q20160418','root','2016-05-16 13:06:39','1',NULL,NULL),(292,'A3:35:DD:DD:DD:DD','192.168.12.3','255.255.255.0','q20160419','root','2016-05-16 13:06:39','1',NULL,NULL),(293,'A3:36:DD:DD:DD:DD','192.168.12.4','255.255.255.0','q20160420','root','2016-05-16 13:06:39','1',NULL,NULL),(294,'A3:37:DD:DD:DD:DD','192.168.12.5','255.255.255.0','q20160421','root','2016-05-16 13:06:39','1',NULL,NULL),(295,'A3:38:DD:DD:DD:DD','192.168.12.6','255.255.255.0','q20160422','root','2016-05-16 13:06:39','1',NULL,NULL),(296,'A3:42:DD:DD:DD:DD','192.168.12.10','255.255.255.0','q20160426','root','2016-05-16 13:06:39','1',NULL,NULL),(297,'A3:43:DD:DD:DD:DD','192.168.12.11','255.255.255.0','q20160427','root','2016-05-16 13:06:39','1',NULL,NULL),(298,'A3:44:DD:DD:DD:DD','192.168.12.12','255.255.255.0','q20160428','root','2016-05-16 13:06:39','1',NULL,NULL),(299,'A3:45:DD:DD:DD:DD','192.168.12.13','255.255.255.0','q20160429','root','2016-05-16 13:06:39','1',NULL,NULL),(300,'A3:48:DD:DD:DD:DD','192.168.12.16','255.255.255.0','q20160432','root','2016-05-16 13:06:39','1',NULL,NULL),(301,'A3:49:DD:DD:DD:DD','192.168.12.17','255.255.255.0','q20160433','root','2016-05-16 13:06:39','1',NULL,NULL),(302,'A3:50:DD:DD:DD:DD','192.168.12.18','255.255.255.0','q20160434','root','2016-05-16 13:06:39','1',NULL,NULL),(303,'A3:51:DD:DD:DD:DD','192.168.12.19','255.255.255.0','q20160435','root','2016-05-16 13:06:39','1',NULL,NULL),(304,'A3:52:DD:DD:DD:DD','192.168.12.20','255.255.255.0','q20160436','root','2016-05-16 13:06:39','1',NULL,NULL),(305,'A3:53:DD:DD:DD:DD','192.168.12.21','255.255.255.0','q20160437','root','2016-05-16 13:06:39','1',NULL,NULL),(306,'A3:54:DD:DD:DD:DD','192.168.12.22','255.255.255.0','q20160438','root','2016-05-16 13:06:39','1',NULL,NULL),(307,'A3:55:DD:DD:DD:DD','192.168.12.23','255.255.255.0','q20160439','root','2016-05-16 13:06:39','1',NULL,NULL),(308,'A3:56:DD:DD:DD:DD','192.168.12.24','255.255.255.0','q20160440','root','2016-05-16 13:06:39','1',NULL,NULL),(309,'A3:57:DD:DD:DD:DD','192.168.12.25','255.255.255.0','q20160441','root','2016-05-16 13:06:39','1',NULL,NULL),(310,'A3:58:DD:DD:DD:DD','192.168.12.26','255.255.255.0','q20160442','root','2016-05-16 13:06:39','1',NULL,NULL),(311,'A3:59:DD:DD:DD:DD','192.168.12.27','255.255.255.0','q20160443','root','2016-05-16 13:06:39','1',NULL,NULL),(312,'A3:60:DD:DD:DD:DD','192.168.12.28','255.255.255.0','q20160444','root','2016-05-16 13:06:39','1',NULL,NULL),(313,'A3:61:DD:DD:DD:DD','192.168.12.29','255.255.255.0','q20160445','root','2016-05-16 13:06:39','1',NULL,NULL),(314,'A3:62:DD:DD:DD:DD','192.168.12.30','255.255.255.0','q20160446','root','2016-05-16 13:06:39','1',NULL,NULL),(315,'A3:63:DD:DD:DD:DD','192.168.12.31','255.255.255.0','q20160447','root','2016-05-16 13:06:39','1',NULL,NULL),(316,'A3:64:DD:DD:DD:DD','192.168.12.32','255.255.255.0','q20160448','root','2016-05-16 13:06:39','1',NULL,NULL),(317,'A3:65:DD:DD:DD:DD','192.168.12.33','255.255.255.0','q20160449','root','2016-05-16 13:06:39','1',NULL,NULL),(318,'A3:66:DD:DD:DD:DD','192.168.12.34','255.255.255.0','q20160450','root','2016-05-16 13:06:39','1',NULL,NULL),(319,'A3:67:DD:DD:DD:DD','192.168.12.35','255.255.255.0','q20160451','root','2016-05-16 13:06:39','1',NULL,NULL),(320,'A3:68:DD:DD:DD:DD','192.168.12.36','255.255.255.0','q20160452','root','2016-05-16 13:06:39','1',NULL,NULL);

/*Table structure for table `inventory_iso` */

DROP TABLE IF EXISTS `inventory_iso`;

CREATE TABLE `inventory_iso` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `ISONAME` varchar(100) NOT NULL,
  `ISOTYPE` varchar(10) DEFAULT NULL,
  `ISOVERSION` varchar(10) DEFAULT NULL,
  `ISOUSERVERSION` varchar(50) DEFAULT NULL,
  `STATUS_BIT` varchar(2) DEFAULT NULL,
  `STATUS_DESCR` varchar(50) DEFAULT NULL,
  `ISOBIT` int(11) DEFAULT NULL,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `USEAIM` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `inventory_iso` */

insert  into `inventory_iso`(`CSAID`,`ISONAME`,`ISOTYPE`,`ISOVERSION`,`ISOUSERVERSION`,`STATUS_BIT`,`STATUS_DESCR`,`ISOBIT`,`USERACCOUNT`,`CREATETIME`,`USEAIM`) values (5,'CentOS-7.0-1406-x86_64','CEN','7.0','ssdfsd','02','上传完成',64,'root','2016-05-13 16:14:32','dsfasd'),(6,'rhel-server-5.5-x86_64','RHEL','5.5','qwewqweqe','02','上传完成',64,'root','2016-05-13 16:20:43','sdfasdff'),(7,'rhel-server-6.6-x86_64','RHEL','6.6','rtet','02','上传完成',64,'root','2016-05-13 16:26:10','向出租车'),(8,'rhel-server-7.1-x86_64','RHEL','7.1','asdasd','02','上传完成',64,'root','2016-05-13 16:31:43','sdfasfsdf'),(9,'RHEL_5.4-x86_64','RHEL','5.4','sdfsdaf','00','上传中',64,'root','2016-05-13 16:36:59','sdfsdafsda');

/*Table structure for table `inventory_iso_server` */

DROP TABLE IF EXISTS `inventory_iso_server`;

CREATE TABLE `inventory_iso_server` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVERURL` varchar(200) NOT NULL,
  `IP` varchar(15) NOT NULL,
  `SERVERTYPE` int(11) NOT NULL,
  `SERVERSTATUSMARK` varchar(20) NOT NULL,
  `UPDATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `inventory_iso_server` */

insert  into `inventory_iso_server`(`CSAID`,`SERVERURL`,`IP`,`SERVERTYPE`,`SERVERSTATUSMARK`,`UPDATETIME`) values (1,'http://192.168.1.14:38061/csaAgent','192.168.1.14',0,'0','2016-05-16 16:37:02');

/*Table structure for table `inventory_ks` */

DROP TABLE IF EXISTS `inventory_ks`;

CREATE TABLE `inventory_ks` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `KSNAME` varchar(100) NOT NULL,
  `ISOCSAID` int(11) NOT NULL,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `UPDATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `inventory_ks` */

insert  into `inventory_ks`(`CSAID`,`KSNAME`,`ISOCSAID`,`USERACCOUNT`,`CREATETIME`,`UPDATETIME`) values (3,'sdfsdf',5,'root','2016-05-16 15:55:31','2016-05-16 15:55:29'),(4,'csdfsd',6,'root','2016-05-16 15:56:35','2016-05-16 15:56:33'),(5,'sdfsdf',9,'root','2016-05-16 15:56:47','2016-05-16 15:56:46');

/*Table structure for table `inventory_package` */

DROP TABLE IF EXISTS `inventory_package`;

CREATE TABLE `inventory_package` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `PACKAGENAME` varchar(500) NOT NULL,
  `GROUPCSAID` int(11) NOT NULL,
  `PACKAGEFULLNAME` varchar(500) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inventory_package` */

/*Table structure for table `inventory_packagegroup` */

DROP TABLE IF EXISTS `inventory_packagegroup`;

CREATE TABLE `inventory_packagegroup` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUPNAME` varchar(100) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inventory_packagegroup` */

/*Table structure for table `inventory_playbook` */

DROP TABLE IF EXISTS `inventory_playbook`;

CREATE TABLE `inventory_playbook` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `PBNAME` varchar(100) NOT NULL,
  `RUNSTATUS` varchar(20) NOT NULL,
  `CRONEXPRESSION` varchar(500) NOT NULL,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREARETIME` datetime NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inventory_playbook` */

/*Table structure for table `inventory_scheme` */

DROP TABLE IF EXISTS `inventory_scheme`;

CREATE TABLE `inventory_scheme` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `KSCSAID` int(11) NOT NULL,
  `SCHEMENAME` varchar(100) NOT NULL,
  `SCHEMESTATUS` int(11) NOT NULL,
  `INSTALLSTATUS` int(11) NOT NULL,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `INSTALLSUM` int(11) DEFAULT NULL,
  `INSTALLFINISHED` int(11) DEFAULT NULL,
  `INSTALLING` int(11) DEFAULT NULL,
  `INSTALLERROR` int(11) DEFAULT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `inventory_scheme` */

insert  into `inventory_scheme`(`CSAID`,`KSCSAID`,`SCHEMENAME`,`SCHEMESTATUS`,`INSTALLSTATUS`,`USERACCOUNT`,`CREATETIME`,`INSTALLSUM`,`INSTALLFINISHED`,`INSTALLING`,`INSTALLERROR`) values (1,1,'1',1,1,'1','2016-05-16 01:42:14',1,1,1,1),(2,2,'2',4,1,'1','2016-05-03 12:18:53',1,1,1,1),(3,3,'3',3,1,'1','2016-05-12 12:19:08',1,1,1,1),(4,3,'3',3,1,'1','2016-05-16 12:19:33',1,1,1,1),(5,4,'3',1,1,'1','2016-05-16 12:19:50',1,1,1,1);

/*Table structure for table `inventory_shell` */

DROP TABLE IF EXISTS `inventory_shell`;

CREATE TABLE `inventory_shell` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `FILENAME` varchar(100) NOT NULL,
  `USERACCOUNT` varchar(100) NOT NULL,
  `FILESIZE` int(11) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `FILETYPE` varchar(100) NOT NULL,
  `UPDATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `inventory_shell` */

insert  into `inventory_shell`(`CSAID`,`FILENAME`,`USERACCOUNT`,`FILESIZE`,`CREATETIME`,`FILETYPE`,`UPDATETIME`) values (1,'hello.yml','root',1,'2016-05-13 18:49:06','软件安装','2016-05-13 18:49:06'),(2,'Noname1.txt','root',1,'2016-05-13 18:49:20','系统部署','2016-05-13 18:49:20'),(3,'db迁移.txt','root',2,'2016-05-16 10:30:56','配置采集','2016-05-16 10:30:55');

/*Table structure for table `inventory_soft` */

DROP TABLE IF EXISTS `inventory_soft`;

CREATE TABLE `inventory_soft` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `SOFTNAME` varchar(100) NOT NULL,
  `GROUPNAME` varchar(20) DEFAULT NULL,
  `USERNAME` varchar(20) DEFAULT NULL,
  `INSTALLPATH` varchar(200) DEFAULT NULL,
  `SOFTPACKAGE` varchar(100) NOT NULL,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inventory_soft` */

/*Table structure for table `log_inventory_iso` */

DROP TABLE IF EXISTS `log_inventory_iso`;

CREATE TABLE `log_inventory_iso` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `USERACCOUNT` varchar(100) NOT NULL,
  `UPDATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERIP` varchar(15) DEFAULT NULL,
  `NOTES` varchar(500) NOT NULL,
  `ISOCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `log_inventory_iso` */

/*Table structure for table `log_ks` */

DROP TABLE IF EXISTS `log_ks`;

CREATE TABLE `log_ks` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `USERACCOUNT` varchar(100) NOT NULL,
  `UPDATETIME` datetime NOT NULL,
  `USERIP` varchar(15) DEFAULT NULL,
  `NOTES` varchar(500) NOT NULL,
  `KICKSTARTCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `log_ks` */

/*Table structure for table `log_os_ip` */

DROP TABLE IF EXISTS `log_os_ip`;

CREATE TABLE `log_os_ip` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `USERACCOUNT` varchar(100) NOT NULL,
  `UPDATETIME` datetime NOT NULL,
  `USERIP` varchar(15) DEFAULT NULL,
  `NOTES` varchar(500) NOT NULL,
  `IPCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

/*Data for the table `log_os_ip` */

insert  into `log_os_ip`(`CSAID`,`USERACCOUNT`,`UPDATETIME`,`USERIP`,`NOTES`,`IPCSAID`) values (109,'root','2016-05-16 13:06:39','127.0.0.1','批量导入时修改！',267),(110,'root','2016-05-16 13:06:39','127.0.0.1','批量导入时修改！',268);

/*Table structure for table `log_playbook` */

DROP TABLE IF EXISTS `log_playbook`;

CREATE TABLE `log_playbook` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `USERIP` varchar(15) NOT NULL,
  `NOTES` varchar(500) NOT NULL,
  `PBCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `log_playbook` */

/*Table structure for table `log_soft` */

DROP TABLE IF EXISTS `log_soft`;

CREATE TABLE `log_soft` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `USERACCOUNT` varchar(100) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `USERIP` varchar(15) NOT NULL,
  `NOTES` varchar(500) NOT NULL,
  `SOFTCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `log_soft` */

/*Table structure for table `rel_scheme_ip` */

DROP TABLE IF EXISTS `rel_scheme_ip`;

CREATE TABLE `rel_scheme_ip` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `SCHCSAID` int(11) NOT NULL,
  `IPCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `rel_scheme_ip` */

/*Table structure for table `rel_scheme_ks` */

DROP TABLE IF EXISTS `rel_scheme_ks`;

CREATE TABLE `rel_scheme_ks` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `SCHCSAID` int(11) NOT NULL,
  `KSCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `rel_scheme_ks` */

/*Table structure for table `rel_shell_ks` */

DROP TABLE IF EXISTS `rel_shell_ks`;

CREATE TABLE `rel_shell_ks` (
  `CSAID` int(11) NOT NULL AUTO_INCREMENT,
  `KSCSAID` int(11) NOT NULL,
  `SHELLCSAID` int(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `rel_shell_ks` */

insert  into `rel_shell_ks`(`CSAID`,`KSCSAID`,`SHELLCSAID`) values (5,3,3),(6,3,2),(7,4,3),(8,5,3);

/*Table structure for table `task_install_soft` */

DROP TABLE IF EXISTS `task_install_soft`;

CREATE TABLE `task_install_soft` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `TASKSTATUS` int(11) DEFAULT NULL,
  `TASKMSG` varchar(2000) DEFAULT NULL,
  `MEDIACSAID` int(11) NOT NULL,
  `TARGETCSAID` int(11) NOT NULL,
  `NOTICESTATUS` int(11) NOT NULL,
  `CREATETIME` datetime NOT NULL,
  `WAREHOUSESERVER` int(11) NOT NULL,
  PRIMARY KEY (`TID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `task_install_soft` */

/*Table structure for table `task_iso` */

DROP TABLE IF EXISTS `task_iso`;

CREATE TABLE `task_iso` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `TASKSTATUS` int(11) NOT NULL,
  `TASKTYPE` int(11) NOT NULL,
  `TASKMSG` varchar(2000) DEFAULT NULL,
  `MEDIACSAID` int(11) DEFAULT NULL,
  `NOTICESTATUS` int(11) NOT NULL,
  `SCHEMECSAID` int(11) DEFAULT NULL,
  `CREATETIME` datetime NOT NULL,
  `WAREHOUSESERVER` int(11) DEFAULT NULL,
  `ENDTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`TID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `task_iso` */

insert  into `task_iso`(`TID`,`TASKSTATUS`,`TASKTYPE`,`TASKMSG`,`MEDIACSAID`,`NOTICESTATUS`,`SCHEMECSAID`,`CREATETIME`,`WAREHOUSESERVER`,`ENDTIME`) values (5,2,1,NULL,5,1,NULL,'2016-05-13 16:39:24',1,NULL),(8,1,1,NULL,6,1,NULL,'2016-05-16 11:27:56',1,NULL),(9,0,1,NULL,7,1,NULL,'2016-05-16 14:12:38',1,NULL),(10,1,1,NULL,8,1,NULL,'2016-05-16 14:54:13',1,'2016-05-16 16:25:56'),(11,0,1,NULL,9,0,NULL,'2016-05-16 16:33:06',1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
